#ifndef UTILS_H
# define UTILS_H

# include "rtv2.h"

# include <sys/types.h>
# include <ctype.h>
# include <unistd.h>
# include <assert.h>
# include <stdarg.h>

# define MAX_SOURCE_SIZE	0x100000
# define NB_ELEMENTS(x)		(sizeof(x) / sizeof(*x))

# define UNUSED				__attribute__((__unused__))

# define RED(x)				((x & 0xFF0000) >> 16)
# define GREEN(x)			((x & 0xFF00) >> 8)
# define BLUE(x)			(x & 0xFF)
# define ALPHA(x)			1	/* ((x & 0xFF000000) >> 24) */
# define RGBA(r, g, b, a)	

typedef unsigned char		uchar;
typedef unsigned short		ushort;
typedef unsigned int		uint;
typedef unsigned long		ulong;
typedef unsigned long long	ullong;

int		clamp(int nbr, int min, int max);
long	clampl(long nbr, long min, long max);
float	clampf(float nbr, float min, float max);
int		printd(UNUSED const char *format, ...);
void	free_str_tab(char **tab);
char	**get_file(char *pathname);
char	**rm_white_lines(char **src);
char	*strjoin(const char *s1, const char *s2);
char	**strsplit(const char *str, char delim);

#endif /* UTILS_H */
