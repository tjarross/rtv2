#ifndef RTV2_H
# define RTV2_H

# define _GNU_SOURCE // needed for strcase*() functions

# include <linux/limits.h>
# include <GL/glew.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <string.h>
# include <stdarg.h>
# include <time.h>

# include "opengl.h"
# include "libcl.h"

# define WIDTH	1280
# define HEIGHT 720

# define WINDOW_TITLE	"RTv2"

# define KERNEL_COMPILE_FLAGS		/*" -cl-fast-relaxed-math"*/ \
									" -cl-no-signed-zeros" \
									" -cl-mad-enable" \
									" -cl-strict-aliasing" \
									" -cl-single-precision-constant" \
									" -Isources/opencl/kernel/"

# define INTERSECTION_KERNEL_NAME	"raytrace"
# define LIGHTNING_KERNEL_NAME		"lightning"
# define FILL_IMAGE_KERNEL_NAME		"fill_image"

typedef enum	e_light_type
{
	POINT,
	PARALLEL
}				t_light_type;

typedef enum	e_obj_type
{
	NONE,
	PLANE,
	CYLINDER,
	SPHERE,
	CONE,
	ELLIPSOID,
	HYPERBOLOID
}				t_obj_type;

typedef struct	s_cam
{
	cl_float3	pos;	// between -FLT_MAX	and	FLT_MAX
	cl_float3	dir;	// between -FLT_MAX	and	FLT_MAX
}				t_cam;

typedef struct	s_world
{
	float		ambient_coef;	// between 0.f		and	1.f
	float		filter_coef;	// between 0.f		and	1.f
	uint		filter_color;	// between 0x000000	and	0xFFFFFF
	uint		antialiasing;	// between 1		and	7
	uint		nb_reflection;	// between 0		and	255
	uint		nb_refraction;	// between 0		and	255

	uint		nb_light;		// between 1	and	4096
	uint		nb_plane;		// between 1	and	4096
	uint		nb_cylinder;	// between 1	and	4096
	uint		nb_sphere;		// between 1	and	4096
	uint		nb_cone;		// between 1	and	4096
	uint		nb_ellipsoid;	// between 1	and	4096
	uint		nb_hyperboloid;	// between 1	and	4096
}				t_world;

typedef struct		s_light
{
	cl_float3		pos;				// between -FLT_MAX	and	FLT_MAX
	t_light_type	type;
	float			luminosity_coef;	// between 0.f		and	1.f
	cl_float4		color;				// between 0x000000	and	0xFFFFFF
}					t_light;

typedef struct	s_plane
{
	cl_float3	pos;				// between -FLT_MAX	and	FLT_MAX
	cl_float3	dir;				// between -FLT_MAX	and	FLT_MAX
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	float		checkerboard_coef;	// between 0.f		and	1.f
	uint		checkerboard_color;	// between 0x000000	and	0xFFFFFF
	cl_float4	color; 				// between 0x000000	and	0xFFFFFF
}				t_plane;

typedef struct	s_cylinder
{
	cl_float3	pos;				// between -FLT_MAX	and	FLT_MAX
	cl_float3	dir;				// between -FLT_MAX	and	FLT_MAX
	float		radius;				// between 0		and	INT_MAX
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	cl_float4	color;				// between 0x000000 and	0xFFFFFF
}				t_cylinder;

typedef struct	s_sphere
{
	cl_float3	pos;				// between -FLT_MAX	and	FLT_MAX
	cl_float3	dir;				// between -FLT_MAX	and	FLT_MAX
	float		radius;				// between 0		and	FLT_MAX
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	cl_float4	color;				// between 0x000000	and	0xFFFFFF
}				t_sphere;

typedef struct	s_cone
{
	cl_float3	pos;				// between -FLT_MAX	and	FLT_MAX
	cl_float3	dir;				// between -FLT_MAX	and	FLT_MAX
	float		angle;				// between 0.f		and	90.f
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	cl_float4	color;				// between 0x000000	and	0xFFFFFF
}				t_cone;

typedef struct	s_ellipsoid
{
	cl_float3	pos;				// between -FLT_MAX	and	FLT_MAX
	cl_float3	dir;				// between -FLT_MAX	and	FLT_MAX
	cl_float3	radius;				// between 0.f		and	90.f
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	cl_float4	color;				// between 0x000000	and	0xFFFFFF
}				t_ellipsoid;

typedef struct	s_hyperboloid
{
	cl_float3	pos;				// between -FLT_MAX	and	FLT_MAX
	cl_float3	dir;				// between -FLT_MAX	and	FLT_MAX
	float		radius;				// between 0		and	FLT_MAX
	float		angle;				// between 0.f		and	90.f
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	cl_float4	color;				// between 0x000000	and	0xFFFFFF
}				t_hyperboloid;

typedef struct 		s_scene
{
	t_cam			cam;
	t_world			world;
	t_light			*light;
	t_plane			*plane;
	t_cylinder		*cylinder;
	t_sphere		*sphere;
	t_cone			*cone;
	t_ellipsoid		*ellipsoid;
	t_hyperboloid	*hyperboloid;
}					t_scene;

typedef struct	s_intersection
{
	cl_float3	coord;
	cl_float3	normal;
	t_obj_type	obj_type;
	union
	{
		t_plane			plane;
		t_cylinder		cylinder;
		t_sphere		sphere;
		t_cone			cone;
		t_ellipsoid		ellipsoid;
		t_hyperboloid	hyperboloid;
	};
	cl_float4	color;
}				t_intersection;

typedef struct	s_opencl_buffer
{
	cl_int		texture;
	cl_int		inters;
	cl_int		light;
	cl_int		plane;
	cl_int		cylinder;
	cl_int		sphere;
	cl_int		cone;
	cl_int		ellipsoid;
	cl_int		hyperboloid;
}				t_opencl_buffer;

cl_int		setup_intersection_kernel(t_display display, t_scene scene);
cl_int		setup_lightning_kernel(t_display display, t_scene scene);
cl_int		setup_fill_image_kernel(t_display display);

#endif /* RTV2_H */
