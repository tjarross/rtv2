#ifndef OPENGL_H
# define OPENGL_H

# include <GL/glew.h>
# include <GLFW/glfw3.h>

# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>

# define LOG_STATUS_BUFFER_LEN	512

# define VERTEX_SHADER_PATH		"sources/opengl/shader/vertex.glsl"
# define FRAGMENT_SHADER_PATH	"sources/opengl/shader/fragment.glsl"

# define VERSION_MAJOR	3
# define VERSION_MINOR	3

typedef struct	s_display
{
	GLFWwindow	*window;
	GLuint		program;
	GLuint		vao;
	GLuint		gl_screen_corners;
	GLuint		gl_corners_ebo;
	GLuint		gl_texture;
	ushort		width;
	ushort		height;
}				t_display;

GLFWwindow	*glfw_init(const char *window_name);
GLuint 		load_program(const char *vertex_shader_path, const char *fragment_shader_path);

void 		key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void		error_callback(int error, const char *msg);

#endif /* OPENGL_H */
