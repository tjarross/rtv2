#ifndef PARSER_H
# define PARSER_H

# include "rtv2.h"
# include "utils.h"

# include <float.h>
# include <limits.h>

# define MAX_ENTITIES		4096

// PARSER/DISPLAY/			DIRECTORY
void		cam_infos(t_scene scn);
void		display_parser_infos(t_scene scn);
void		light_infos(t_scene scn);
void		plane_infos(t_scene scn);
void		cylinder_infos(t_scene scn);
void		sphere_infos(t_scene scn);
void		cone_infos(t_scene scn);
void		ellipsoid_infos(t_scene scn);
void		hyperboloid_infos(t_scene scn);
void		world_infos(t_scene scn);

// PARSER/ENTITIES/			DIRECTORY
t_cam			get_cam(char **file, uint i);
t_light			*get_light(uint *nb_light, char **file, uint i);
t_plane			*get_plane(uint *nb_plane, char **file, uint i);
t_cylinder		*get_cylinder(uint *nb_cylinder, char **file, uint i);
t_sphere		*get_sphere(uint *nb_sphere, char **file, uint i);
t_cone			*get_cone(uint *nb_cone, char **file, uint i);
t_ellipsoid		*get_ellipsoid(uint *nb_ellipsoid, char **file, uint i);
t_hyperboloid	*get_hyperboloid(uint *nb_hyperboloid, char **file, uint i);
t_world			get_world(char **file, uint i);

// PARSER/					DIRECTORY
void		fill_vector3f(cl_float3 *field, char **scene, uint i);
long		get_number_entity(char *line);
uint		get_number_index(char *line);
t_scene		parser(char *pathname);

#endif /* PARSER_H */
