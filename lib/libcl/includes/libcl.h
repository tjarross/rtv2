#ifndef LIBCL_H
# define LIBCL_H

# include <stdlib.h>
# include <stdio.h>
# include <assert.h>
# include <string.h>

# define LIBCL_ENABLE_INTEROP

# ifdef LIBCL_ENABLE_INTEROP
#  include "GLFW/glfw3.h"
# endif /* LIBCL_ENABLE_INTEROP */

# ifdef __APPLE__
#  ifdef LIBCL_ENABLE_INTEROP
#   include <OpenGL/CGLDevice.h>
#   include <OpenGL/CGLCurrent.h>
#  endif /* LIBCL_ENABLE_INTEROP */
#  include <OpenCL/opencl.h>
# else
#  ifdef LIBCL_ENABLE_INTEROP
#   include <CL/cl_gl.h>
#   define GLFW_EXPOSE_NATIVE_X11
#   define GLFW_EXPOSE_NATIVE_GLX
#   include <GLFW/glfw3native.h>
#  endif /* LIBCL_ENABLE_INTEROP */
#  include <CL/cl.h>
# endif /* __APPLE__ */

#define MAX_SRC_SIZE	0x100000

typedef struct	s_ext_var
{
	int			init;
	int			kernel_count;
	int			cl_buffer_count;
#  ifdef LIBCL_ENABLE_INTEROP
	int			gl_buffer_count;
#  endif /* LIBCL_ENABLE_INTEROP */
	FILE		*exception_fp;
}				t_ext_var;

typedef struct			s_cl
{
	cl_platform_id		platform_id;
	cl_device_id		device_id;
	cl_context			context;
	cl_command_queue	command_queue;
	cl_program			program;
	cl_kernel			*kernel;
	cl_mem				*cl_buffer;
#  ifdef LIBCL_ENABLE_INTEROP
	cl_mem				*gl_buffer;
#  endif /* LIBCL_ENABLE_INTEROP */
	cl_int				ret;
	t_ext_var			ext_var;
}						t_cl;

t_cl	cl;

int		cl_init(void);
cl_int	cl_create_kernel(const char **filename, unsigned int nb_files, const char *kernel_name, const char *compile_flags);
int		cl_delete_kernel(cl_int kernel);
cl_int	cl_create_buffer(void *buffer, cl_mem_flags flags, size_t buf_size);
cl_int	cl_create_empty_buffer(cl_mem_flags flags, size_t buf_size);
int		cl_delete_buffer(cl_int buffer);
int		cl_set_buffer_arg(cl_int kernel, cl_uint param_pos, cl_int buffer);
int		cl_set_value_arg(cl_int kernel, cl_uint param_pos, size_t size, void *param);
int		cl_exec_kernel(cl_int kernel, size_t cores);
int		cl_read_buffer(cl_int buffer, void *ret_buffer, size_t offset, size_t size);
int		cl_finish_kernel_exec(void);
int		cl_exit(void);

int		cl_exception(const char *error, cl_int ret);

# ifdef LIBCL_ENABLE_INTEROP

int		cl_interop_init(GLFWwindow *window);
cl_int	cl_create_gl_buffer(GLuint buffer, cl_mem_flags flags);
cl_int	cl_create_gl_texture(GLuint texture, cl_mem_flags flags, GLenum texture_target, GLint miplevel);
int		cl_delete_gl_buffer(cl_int buffer);
int		cl_set_gl_buffer_arg(cl_int kernel, cl_uint param_pos, cl_int buffer);
int		cl_exec_interop_kernel(cl_int kernel, size_t cores);

# endif /* LIBCL_ENABLE_INTEROP */

#endif /* LIBCL_H */
