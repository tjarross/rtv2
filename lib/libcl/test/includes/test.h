#ifndef TEST_H
# define TEST_H

#include "../../includes/libcl.h"

#define MAX_F_NAME	128

typedef struct	s_test
{
	int			failed_tests;
	int			successful_tests;
	int			counter;
	char		f_name[MAX_F_NAME];
}				t_test;

t_test	t;

void	init_test(const char *function);
void	test(int f_ret, int expected_ret);
void	finish_tests();

void	test_cl_init();
void	test_cl_create_kernel();
void	test_cl_kernel_count();
void	test_cl_delete_kernel();
void	test_cl_create_buffer();
void	test_cl_buffer_count();
void	test_cl_create_empty_buffer();
void	test_cl_delete_buffer();
void	test_cl_set_buffer_arg();
void	test_cl_set_value_arg();
void	test_cl_exec_kernel();
void	test_cl_read_buffer();
void	test_cl_finish_kernel_exec();
void	test_cl_exit();

# ifdef LIBCL_ENABLE_INTEROP
void	test_cl_interop_init();
void	test_cl_create_gl_buffer();
void	test_gl_buffer_count();
void	test_cl_delete_gl_buffer();
void	test_cl_set_gl_buffer_arg();
void	test_cl_exec_interop_kernel();
# endif /* LIBCL_ENABLE_INTEROP */

#endif /* TEST_H */
