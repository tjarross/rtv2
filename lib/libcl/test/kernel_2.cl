__kernel void	kern_s(__global char *str)
{
	str[0] = 'H';
	str[1] = 'e';
	str[2] = 'l';
	str[3] = 'l';
	str[4] = 'o';
	str[5] = ' ';
	str[6] = 'K';
	str[7] = 'e';
	str[8] = 'r';
	str[9] = 'n';
	str[10] = 'e';
	str[11] = 'l';
	str[12] = '!';
	str[13] = '\n';
}

__kernel void	kern(__global char *str)
{
	str[0] = 'H';
	str[1] = 'e';
	str[2] = 'l';
	str[3] = 'l';
	str[4] = 'o';
	str[5] = ' ';
	str[6] = 'W';
	str[7] = 'o';
	str[8] = 'r';
	str[9] = 'l';
	str[10] = 'd';
	str[11] = ' ';
	str[12] = '!';
	str[13] = '\n';
}
