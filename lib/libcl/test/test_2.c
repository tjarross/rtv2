#include "../includes/libcl.h"

int main(void)
{
	char	*str = malloc(15);
	char	*str2 = malloc(15);
	cl_int	kern;
	cl_int	kern_s;
	cl_int	buffer;
	cl_int	buffer_s;

	str[14] = '\0';
	str2[14] = '\0';
	cl_init();
	kern = cl_create_kernel("test/kernel_2.cl", "kern");
	kern_s = cl_create_kernel("test/kernel_2.cl", "kern_s");
	buffer = cl_create_buffer(str, CL_MEM_WRITE_ONLY, 14);
	buffer_s = cl_create_buffer(str2, CL_MEM_WRITE_ONLY, 14);
	cl_set_buffer_arg(kern, 0, buffer);
	cl_set_buffer_arg(kern_s, 0, buffer_s);
	cl_exec_kernel(kern, 1);
	cl_exec_kernel(kern_s, 1);
	cl_read_buffer(buffer, str, 14);
	cl_read_buffer(buffer_s, str2, 14);
	cl_finish_kernel_exec();
	printf("%s%s", str, str2);
	cl_exit();
	free(str);
	free(str2);
	return (0);
}
