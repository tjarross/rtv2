#include "../includes/libcl.h"

typedef struct	s_test
{
	int		test_1;
	float	test_2;
}				t_test;

int main(void)
{
	char	*str = malloc(15);
	t_test	*t = malloc(sizeof(*t) * 2);
	cl_int	kernel;
	cl_int	buffer;
	int		ret;

	t[0].test_1 = 0;
	t[0].test_2 = 0.f;
	t[1].test_1 = 0;
	t[1].test_2 = 0.f;
	str[14] = '\0';
	ret = cl_init();
	kernel = cl_create_kernel("kernel.cl", "kern");
	buffer = cl_create_buffer(t, CL_MEM_WRITE_ONLY, sizeof(*t) * 2);
	ret = cl_set_buffer_arg(kernel, 0, buffer);
	ret = cl_exec_kernel(kernel, 1);
	ret = cl_read_buffer(buffer, t, sizeof(*t) * 2);
	ret = cl_finish_kernel_exec();
	printf("%d\n", t[0].test_1);
	printf("%f\n", t[0].test_2);
	printf("%d\n", t[1].test_1);
	printf("%f\n", t[1].test_2);
	ret = cl_exit();
	return (0);
}
