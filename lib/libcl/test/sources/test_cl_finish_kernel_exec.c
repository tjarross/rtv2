#include "test.h"

void	test_cl_finish_kernel_exec()
{
	init_test("cl_finish_kernel_exec");
	cl_init();
	test(cl_finish_kernel_exec(), 0);
	cl_exit();
}
