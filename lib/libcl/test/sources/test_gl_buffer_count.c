#include "test.h"

void	test_gl_buffer_count()
{
	char	*str = malloc(42);

	gl_create_buffer(NULL, CL_MEM_WRITE_ONLY, 0);
	test("1 gl_buffer_count", cl.ext_var.gl_buffer_count, 0);
	gl_create_buffer(str, CL_MEM_WRITE_ONLY, 0);
	test("2 gl_buffer_count", cl.ext_var.gl_buffer_count, 0);
	gl_create_buffer(str, CL_MEM_WRITE_ONLY, 42);
	test("3 gl_buffer_count", cl.ext_var.gl_buffer_count, 1);
	gl_create_buffer(NULL, CL_MEM_READ_ONLY, 42);
	test("4 gl_buffer_count", cl.ext_var.gl_buffer_count, 1);
	gl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	test("5 gl_buffer_count", cl.ext_var.gl_buffer_count, 2);
	gl_create_buffer(str, -1, 42);
	test("6 gl_buffer_count", cl.ext_var.gl_buffer_count, 2);
	cl_delete_gl_buffer(1);
	test("7 gl_buffer_count", cl.ext_var.gl_buffer_count, 1);
	gl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	test("8 gl_buffer_count", cl.ext_var.gl_buffer_count, 2);
	cl_delete_gl_buffer(0);
	test("9 gl_buffer_count", cl.ext_var.gl_buffer_count, 2);
	gl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	test("10 gl_buffer_count", cl.ext_var.gl_buffer_count, 2);
	cl_delete_gl_buffer(1);
	test("11 gl_buffer_count", cl.ext_var.gl_buffer_count, 1);
	cl_delete_gl_buffer(0);
	test("11 gl_buffer_count", cl.ext_var.gl_buffer_count, 0);

	gl_create_empty_buffer(CL_MEM_WRITE_ONLY, 0);
	test("11 gl_buffer_count", cl.ext_var.gl_buffer_count, 0);
	gl_create_empty_buffer(CL_MEM_WRITE_ONLY, 0);
	test("12 gl_buffer_count", cl.ext_var.gl_buffer_count, 0);
	gl_create_empty_buffer(CL_MEM_WRITE_ONLY, 42);
	test("13 gl_buffer_count", cl.ext_var.gl_buffer_count, 1);
	gl_create_empty_buffer(CL_MEM_READ_ONLY, 42);
	test("14 gl_buffer_count", cl.ext_var.gl_buffer_count, 2);
	gl_create_empty_buffer(CL_MEM_READ_WRITE, 42);
	test("15 gl_buffer_count", cl.ext_var.gl_buffer_count, 3);
	gl_create_empty_buffer(-1, 42);
	test("16 gl_buffer_count", cl.ext_var.gl_buffer_count, 3);
	cl_delete_gl_buffer(1);
	test("17 gl_buffer_count", cl.ext_var.gl_buffer_count, 3);
	gl_create_empty_buffer(CL_MEM_READ_WRITE, 42);
	test("18 gl_buffer_count", cl.ext_var.gl_buffer_count, 3);
	cl_delete_gl_buffer(0);
	test("19 gl_buffer_count", cl.ext_var.gl_buffer_count, 3);
	gl_create_empty_buffer(CL_MEM_READ_WRITE, 42);
	test("20 gl_buffer_count", cl.ext_var.gl_buffer_count, 3);
	cl_delete_gl_buffer(1);
