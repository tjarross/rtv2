#include "test.h"

void	test_cl_create_buffer()
{
	char	*str = malloc(42);
 
	init_test("cl_create_buffer");
	cl_init();
	test(cl_create_buffer(NULL, CL_MEM_WRITE_ONLY, 0), -1);
	test(cl_create_buffer(str, CL_MEM_WRITE_ONLY, 0), -1);
	test(cl_create_buffer(str, CL_MEM_WRITE_ONLY, 42), 0);
	test(cl_create_buffer(NULL, CL_MEM_READ_ONLY, 42), -1);
	test(cl_create_buffer(str, CL_MEM_READ_WRITE, 42), 1);
	test(cl_create_buffer(str, -1, 42), -1);
	cl_delete_buffer(1);
	test(cl_create_buffer(str, CL_MEM_READ_WRITE, 42), 1);
	cl_delete_buffer(0);
	test(cl_create_buffer(str, CL_MEM_READ_WRITE, 42), 0);
	cl_delete_buffer(1);
	cl_delete_buffer(0);
	cl_exit();
}
