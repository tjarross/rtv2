#include "test.h"

void	test_cl_create_kernel()
{
	const char	*files[] =
	{
		"qwerty",
		"sources/kernel/test_1.cl",
		"sources/kernel/test_2.cl"
	};

	init_test("cl_create_kernel");
	cl_init();
	test(cl_create_kernel(NULL, 0, NULL), -1);
	test(cl_create_kernel(files, 1, "qwerty"), -1);
	test(cl_create_kernel(files + 1, 1, "test"), 0);
	test(cl_create_kernel(files + 1, 1, "test"), 1);
	test(cl_create_kernel(files + 1, 1, "test_2"), 2);
	test(cl_create_kernel(files + 2, 1, "test"), 3);
	cl_delete_kernel(2);
	test(cl_create_kernel(files + 2, 1, "test"), 2);
	cl_delete_kernel(3);
	test(cl_create_kernel(files + 2, 1, "test"), 3);
	cl_delete_kernel(3);
	cl_delete_kernel(2);
	cl_delete_kernel(1);
	cl_delete_kernel(0);
	cl_exit();
}
