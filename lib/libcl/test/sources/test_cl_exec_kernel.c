#include "test.h"

void	test_cl_exec_kernel()
{
	cl_int		kernel;
	cl_int		buffer;
	int			value = 42; 
	const char	*files[] =
	{
		"sources/kernel/test_1.cl"
	};

	init_test("cl_exec_kernel");
	cl_init();
	test(cl_exec_kernel(128, 0), -1);
	test(cl_exec_kernel(128, 42), -1);
	test(cl_exec_kernel(0, 42), -1);
	kernel = cl_create_kernel(files, 1, "test");
	test(cl_exec_kernel(kernel, 0), -1);
	test(cl_exec_kernel(kernel, 42), -1);
	buffer = cl_create_empty_buffer(CL_MEM_READ_WRITE, 42);
	cl_set_buffer_arg(kernel, 0, buffer);
	test(cl_exec_kernel(kernel, 42), 0);
	cl_delete_kernel(kernel);
	kernel = cl_create_kernel(files, 1, "test_3");
	cl_set_buffer_arg(kernel, 0, buffer);
	test(cl_exec_kernel(kernel, 42), -1);
	cl_set_value_arg(kernel, 1, sizeof(int), &value);
	test(cl_exec_kernel(kernel, 42), 0);
	cl_finish_kernel_exec();
	cl_delete_kernel(kernel);
	cl_delete_buffer(buffer);
	cl_exit();
}
