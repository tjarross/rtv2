#include "test.h"

void	test_cl_kernel_count()
{
	const char	*files[] =
	{
		"qwerty",
		"sources/kernel/test_1.cl",
		"sources/kernel/test_2.cl"
	};

	init_test("cl_kernel_count");
	cl_init();
	cl_create_kernel(NULL, 0, NULL);
	test(cl.ext_var.kernel_count, 0);
	cl_create_kernel(files, 1, "qwerty");
	test(cl.ext_var.kernel_count, 0);
	cl_create_kernel(files + 1, 1, "test");
	test(cl.ext_var.kernel_count, 1);
	cl_create_kernel(files + 1, 1, "test");
	test(cl.ext_var.kernel_count, 2);
	cl_create_kernel(files + 1, 1, "test_2");
	test(cl.ext_var.kernel_count, 3);
	cl_create_kernel(files + 2, 1, "test");
	test(cl.ext_var.kernel_count, 4);
	cl_delete_kernel(2);
	test(cl.ext_var.kernel_count, 4);
	cl_create_kernel(files + 2, 1, "test");
	test(cl.ext_var.kernel_count, 4);
	cl_delete_kernel(3);
	test(cl.ext_var.kernel_count, 3);
	cl_create_kernel(files + 2, 1, "test");
	test(cl.ext_var.kernel_count, 4);
	cl_delete_kernel(3);
	test(cl.ext_var.kernel_count, 3);
	cl_delete_kernel(2);
	test(cl.ext_var.kernel_count, 2);
	cl_delete_kernel(1);
	test(cl.ext_var.kernel_count, 1);
	cl_delete_kernel(0);
	test(cl.ext_var.kernel_count, 0);
	cl_exit();
}
