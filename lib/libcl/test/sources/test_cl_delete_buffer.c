#include "test.h"

void	test_cl_delete_buffer()
{
	char	*str = malloc(42);

	init_test("cl_delete_buffer");
	cl_init();
	test(cl_delete_buffer(0), -1);
	test(cl_delete_buffer(42), -1);
	cl_create_buffer(NULL, CL_MEM_WRITE_ONLY, 0);
	test(cl_delete_buffer(1), -1);
	cl_create_buffer(str, CL_MEM_WRITE_ONLY, 42);
	test(cl_delete_buffer(0), 0);
	cl_create_buffer(str, CL_MEM_WRITE_ONLY, 42);
	test(cl_delete_buffer(1), -1);
	test(cl_delete_buffer(0), 0);
	cl_create_buffer(NULL, CL_MEM_READ_ONLY, 42);
	cl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	cl_create_buffer(str, -1, 42);
	cl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	cl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	test(cl_delete_buffer(4), -1);
	test(cl_delete_buffer(0), 0);
	test(cl_delete_buffer(1), 0);
	test(cl_delete_buffer(2), 0);
	test(cl_delete_buffer(3), -1);
	test(cl_delete_buffer(-1), -1);
	cl_exit();
}
