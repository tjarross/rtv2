#include "test.h"

void	test_cl_delete_kernel()
{
	const char	*files[] =
	{
		"qwerty",
		"sources/kernel/test_1.cl",
		"sources/kernel/test_2.cl"
	};

	init_test("cl_delete_kernel");
	cl_init();
	test(cl_delete_kernel(128), -1);
	cl_create_kernel(NULL, 0, NULL);
	cl_create_kernel(files, 1, "qwerty");
	cl_create_kernel(files + 1, 1, "test");
	cl_create_kernel(files + 1, 1, "test");
	cl_create_kernel(files + 1, 1, "test_2");
	cl_create_kernel(files + 2, 1, "test");
	test(cl_delete_kernel(2), 0);
	cl_create_kernel(files + 2, 1, "test");
	test(cl_delete_kernel(3), 0);
	cl_create_kernel(files + 2, 1, "test");
	test(cl_delete_kernel(3), 0);
	test(cl_delete_kernel(2), 0);
	test(cl_delete_kernel(1), 0);
	test(cl_delete_kernel(0), 0);
	cl_exit();
}
