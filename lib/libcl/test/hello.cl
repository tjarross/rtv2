__kernel void	kern(__global char *hello)
{
	hello[0] = 'H';
	hello[1] = 'e';
	hello[2] = 'l';
	hello[3] = 'l';
	hello[4] = 'o';
	hello[5] = ' ';
	hello[6] = 'W';
	hello[7] = 'o';
	hello[8] = 'r';
	hello[9] = 'l';
	hello[10] = 'd';
	hello[11] = ' ';
	hello[12] = '!';
	hello[13] = '\n';
	hello[14] = '\0';
}
