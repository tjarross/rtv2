typedef struct s_test
{
	int			test_1;
	float		test_2;
}				t_test;

__kernel void	kern(__global t_test *test)
{
	test[0].test_1 = 42;
	test[0].test_2 = 0.5;
	test[1].test_1 = 666;
	test[1].test_2 = 0.1337;
}
