#include "libcl.h"

#ifdef LIBCL_ENABLE_INTEROP

int		cl_interop_init(__attribute__((__unused__)) GLFWwindow *window)
{
	if (cl.ext_var.init)
		return (-1);
	bzero(&cl, sizeof(t_cl));
	if ((cl.ret = clGetPlatformIDs(1, &cl.platform_id, NULL)) != CL_SUCCESS)
		return (cl_exception("clGetPlatformIDs() Error:", cl.ret));
#ifdef __APPLE__
	cl_context_properties props[] =
	{
		CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE,
		(cl_context_properties)CGLGetShareGroup(CGLGetCurrentContext()),
		0
	};
	cl.context = clCreateContext(props, 1, NULL, NULL, NULL, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clGetDeviceIDs() Error: ", cl.ret));
	cl.ret = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_DEFAULT, 1, &cl.device_id, NULL);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateContext() Error: ", cl.ret));
	cl.command_queue = clCreateCommandQueue(cl.context, cl.device_id, 0, &cl.ret);
#else
	cl_context_properties props[] =
	{
		CL_GL_CONTEXT_KHR, (cl_context_properties)glfwGetGLXContext(window),
		CL_GLX_DISPLAY_KHR, (cl_context_properties)glfwGetX11Display(),
		CL_CONTEXT_PLATFORM, (cl_context_properties)cl.platform_id,
		0
	};
	if ((cl.ret = clGetGLContextInfoKHR(props, CL_CURRENT_DEVICE_FOR_GL_CONTEXT_KHR,
					sizeof(cl_device_id), &cl.device_id, NULL)) != CL_SUCCESS)
		return (cl_exception("clGetGLContextInfoKHR() Error: ", cl.ret));
	cl.context = clCreateContext(props, 1, &cl.device_id, NULL, NULL, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateContext() Error: ", cl.ret));
	cl.command_queue = clCreateCommandQueueWithProperties(cl.context, cl.device_id, 0, &cl.ret);
#endif /* __APPLE__ */
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateCommandQueue() Error: ", cl.ret));
	cl.ext_var.init = 1;
	return (0);
}

#endif /* LIBCL_ENABLE_INTEROP */

int		cl_init(void)
{
	bzero(&cl, sizeof(t_cl));
	if ((cl.ret = clGetPlatformIDs(1, &cl.platform_id, NULL)) != CL_SUCCESS)
		return (cl_exception("clGetPlatformIDs() Error:", cl.ret));
	cl.ret = clGetDeviceIDs(cl.platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &cl.device_id, NULL);
	cl.context = clCreateContext(NULL, 1, &cl.device_id, NULL, NULL, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clGetDeviceIDs() Error: ", cl.ret));
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateContext() Error: ", cl.ret));
#ifdef __APPLE__
	cl.command_queue = clCreateCommandQueue(cl.context, cl.device_id, 0, &cl.ret);
#else
	cl.command_queue = clCreateCommandQueueWithProperties(cl.context, cl.device_id, 0, &cl.ret);
#endif
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateCommandQueue() Error: ", cl.ret));
	return (0);
}
