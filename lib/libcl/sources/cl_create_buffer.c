#include "libcl.h"

#ifdef LIBCL_ENABLE_INTEROP

cl_int	cl_create_gl_buffer(GLuint buffer, cl_mem_flags flags)
{
	cl_mem	buf;
	int		i = -1;

	while (++i < cl.ext_var.gl_buffer_count)
		if (cl.gl_buffer[i] == NULL)
			break ;
	buf = clCreateFromGLBuffer(cl.context, flags | CL_MEM_COPY_HOST_PTR, buffer, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateFromGLBuffer() Error: ", cl.ret));
	if (!(cl.gl_buffer = realloc(cl.gl_buffer, sizeof(cl_mem) * (cl.ext_var.gl_buffer_count + 1))))
		return (cl_exception("realloc() Error: ", 1));
	cl.gl_buffer[i] = buf;
	++cl.ext_var.gl_buffer_count;
	return (i);
}

cl_int	cl_create_gl_texture(GLuint texture, cl_mem_flags flags, GLenum texture_target, GLint miplevel)
{
	cl_mem	buf;
	int		i = -1;

	while (++i < cl.ext_var.gl_buffer_count)
		if (cl.gl_buffer[i] == NULL)
			break ;
	buf = clCreateFromGLTexture(cl.context, flags & CL_MEM_COPY_HOST_PTR, texture_target, miplevel, texture, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateFromGLTexture() Error: ", cl.ret));
	if (!(cl.gl_buffer = realloc(cl.gl_buffer, sizeof(cl_mem) * (cl.ext_var.gl_buffer_count + 1))))
		return (cl_exception("realloc() Error: ", 1));
	cl.gl_buffer[i] = buf;
	++cl.ext_var.gl_buffer_count;
	return (i);
}

#endif /* LIBCL_ENABLE_INTEROP */

cl_int	cl_create_buffer(void *buffer, cl_mem_flags flags, size_t buf_size)
{
	cl_mem	buf;
	int		i = -1;

	if (buffer == NULL)
		return (cl_create_empty_buffer(flags, buf_size));
	while (++i < cl.ext_var.cl_buffer_count)
		if (cl.cl_buffer[i] == NULL)
			break ;
	buf = clCreateBuffer(cl.context, flags | CL_MEM_COPY_HOST_PTR, buf_size, buffer, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateBuffer() Error: ", cl.ret));
	if (!(cl.cl_buffer = realloc(cl.cl_buffer, sizeof(cl_mem) * (cl.ext_var.cl_buffer_count + 1))))
		return (cl_exception("realloc() Error: ", 1));
	cl.cl_buffer[i] = buf;
	if (i == cl.ext_var.cl_buffer_count)
		++cl.ext_var.cl_buffer_count;
	return (i);
}

cl_int	cl_create_empty_buffer(cl_mem_flags flags, size_t size)
{
	cl_mem	buf;
	int		i = -1;

	while (++i < cl.ext_var.cl_buffer_count)
		if (cl.cl_buffer[i] == NULL)
			break ;
	buf = clCreateBuffer(cl.context, flags, size, NULL, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateBuffer() Error: ", cl.ret));
	if (!(cl.cl_buffer = realloc(cl.cl_buffer, sizeof(cl_mem) * (cl.ext_var.cl_buffer_count + 1))))
		return (cl_exception("realloc() Error: ", 1));
	cl.cl_buffer[i] = buf;
	if (i == cl.ext_var.cl_buffer_count)
		++cl.ext_var.cl_buffer_count;
	return (i);
}
