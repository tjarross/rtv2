#include "libcl.h"

#ifdef LIBCL_ENABLE_INTEROP

int		cl_set_gl_buffer_arg(cl_int kernel, cl_uint param_pos, cl_int buffer)
{
	if (kernel < 0 || kernel >= cl.ext_var.kernel_count)
		return (-1);
	if (buffer < 0 || buffer >= cl.ext_var.gl_buffer_count)
		return (-1);
	if ((cl.ret = clSetKernelArg(cl.kernel[kernel], param_pos, sizeof(cl_mem), &cl.gl_buffer[buffer])) != CL_SUCCESS)
		return (cl_exception("clSetKernelArg() Error: ", cl.ret));
	return (0);
}

#endif /* LIBCL_ENABLE_INTEROP */

int		cl_set_buffer_arg(cl_int kernel, cl_uint param_pos, cl_int buffer)
{
	if (kernel < 0 || kernel >= cl.ext_var.kernel_count)
		return (-1);
	if (buffer < 0 || buffer >= cl.ext_var.cl_buffer_count)
		return (-1);
	if ((cl.ret = clSetKernelArg(cl.kernel[kernel], param_pos, sizeof(cl_mem), &cl.cl_buffer[buffer])) != CL_SUCCESS)
		return (cl_exception("clSetKernelArg() Error: ", cl.ret));
	return (0);
}

int		cl_set_value_arg(cl_int kernel, cl_uint param_pos, size_t size, void *param)
{
	if (kernel < 0 || kernel >= cl.ext_var.kernel_count)
		return (-1);
	if ((cl.ret = clSetKernelArg(cl.kernel[kernel], param_pos, size, param)) != CL_SUCCESS)
		return (cl_exception("clSetKernelArg() Error: ", cl.ret));
	return (0);}
