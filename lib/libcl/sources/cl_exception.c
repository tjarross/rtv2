#include "libcl.h"

int		cl_exception(const char *error, cl_int ret)
{
	if (!cl.ext_var.exception_fp)
		assert((cl.ext_var.exception_fp = fopen("libcl.log", "a")));
	fprintf(cl.ext_var.exception_fp, "%s%d\n", error, ret);
	fclose(cl.ext_var.exception_fp);
	cl.ext_var.exception_fp = NULL;
	return (-1);
}
