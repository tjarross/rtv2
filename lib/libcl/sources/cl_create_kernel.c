#include "libcl.h"

cl_int		cl_create_kernel(const char **filename, unsigned int nb_files, const char *kernel_name, const char *compile_flags)
{
	FILE		*fp;
	char		**source;
	char		error[MAX_SRC_SIZE];
	size_t		*source_size;
	cl_kernel	kernel;
	int			i = -1;

	while (++i < cl.ext_var.kernel_count)
		if (cl.kernel[i] == NULL)
			break ;
	if (!(source = (char **)malloc(sizeof(char *) * nb_files)))
		return (cl_exception("malloc() Error: ", 1));
	if (!(source_size = (size_t *)malloc(sizeof(size_t) * nb_files)))
		return (cl_exception("malloc() Error: ", 1));
	for (unsigned int i = 0; i < nb_files; ++i)
	{
		if (!(source[i] = (char *)malloc(sizeof(char) * MAX_SRC_SIZE)))
			return (cl_exception("malloc() Error: ", 1));
		if (!(fp = fopen(filename[i], "r")))
			return (cl_exception("fopen() Error: ", 1));
		if ((source_size[i] = fread(source[i], 1, MAX_SRC_SIZE, fp)) == 0)
			return (cl_exception("fread() Error: ", 1));
		fclose(fp);
	}
	cl.program = clCreateProgramWithSource(cl.context, nb_files,
			(const char **)source, (const size_t *)source_size, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateProgramWithSource() Error: ", cl.ret));
	for (unsigned int i = 0; i < nb_files; ++i)
		free(source[i]);
	free(source);
	if ((cl.ret = clBuildProgram(cl.program, 1, &cl.device_id, compile_flags, NULL, NULL)) != CL_SUCCESS)
	{
		cl.ret = cl_exception("clBuildProgram() Error: ", cl.ret);
		if ((cl.ret = clGetProgramBuildInfo(cl.program, cl.device_id,
				CL_PROGRAM_BUILD_LOG, MAX_SRC_SIZE, error, NULL)) != CL_SUCCESS)
			return (cl_exception("clGetProgramBuildInfo() Error: ", cl.ret));
		printf("%s\n", error);
		return (-1);
	}
	kernel = clCreateKernel(cl.program, kernel_name, &cl.ret);
	if (cl.ret != CL_SUCCESS)
		return (cl_exception("clCreateKernel() Error: ", cl.ret));
	if (!(cl.kernel = realloc(cl.kernel, sizeof(cl_kernel) * (cl.ext_var.kernel_count + 1))))
		return (cl_exception("realloc() Error: ", 1));
	cl.kernel[i] = kernel;
	if (i == cl.ext_var.kernel_count)
		++cl.ext_var.kernel_count;
	return (i);
}
