#include "libcl.h"

int		cl_read_buffer(cl_int buffer, void *ret_buffer, size_t offset, size_t size)
{
	if (buffer < 0 || buffer >= cl.ext_var.cl_buffer_count || !size)
		return (-1);
	if ((cl.ret = clEnqueueReadBuffer(cl.command_queue, cl.cl_buffer[buffer], CL_FALSE, offset, size, ret_buffer, 0, NULL, NULL)) != CL_SUCCESS)
		return (cl_exception("clEnqueueReadBuffer() Error: ", cl.ret));
	return (0);
}
