#include "rtv2.h"
#include "opengl.h"
#include "utils.h"

extern t_opencl_buffer buffer;

cl_int setup_intersection_kernel ( t_display display, t_scene scene )
{
	int			ret = 0;
	cl_int		kernel;

	const char	*kernel_files[] =
	{
		"sources/opencl/kernel/normal.cl",
		"sources/opencl/kernel/intersections.cl",
		"sources/opencl/kernel/raytrace.cl",
	};

	kernel = cl_create_kernel(kernel_files, NB_ELEMENTS(kernel_files), INTERSECTION_KERNEL_NAME, KERNEL_COMPILE_FLAGS);

	buffer.inters = cl_create_empty_buffer(CL_MEM_WRITE_ONLY, sizeof(t_intersection) * display.width * display.height);
	ret |= cl_set_buffer_arg(kernel, 0, buffer.inters);

	ret |= cl_set_value_arg(kernel, 1, sizeof(display.width), &display.width);
	ret |= cl_set_value_arg(kernel, 2, sizeof(display.height), &display.height);
	ret |= cl_set_value_arg(kernel, 3, sizeof(scene.world), &scene.world);
	ret |= cl_set_value_arg(kernel, 4, sizeof(scene.cam), &scene.cam);

	buffer.plane = cl_create_buffer(scene.plane, CL_MEM_READ_ONLY, sizeof(*scene.plane) * (scene.world.nb_plane ? scene.world.nb_plane : 1));
	ret |= cl_set_buffer_arg(kernel, 5, buffer.plane);

	buffer.cylinder = cl_create_buffer(scene.cylinder, CL_MEM_READ_ONLY, sizeof(*scene.cylinder) * (scene.world.nb_cylinder ? scene.world.nb_cylinder : 1));
	ret |= cl_set_buffer_arg(kernel, 6, buffer.cylinder);

	buffer.sphere = cl_create_buffer(scene.sphere, CL_MEM_READ_ONLY, sizeof(*scene.sphere) * (scene.world.nb_sphere ? scene.world.nb_sphere : 1));
	ret |= cl_set_buffer_arg(kernel, 7, buffer.sphere);

	buffer.cone = cl_create_buffer(scene.cone, CL_MEM_READ_ONLY, sizeof(*scene.cone) * (scene.world.nb_cone ? scene.world.nb_cone : 1));
	ret |= cl_set_buffer_arg(kernel, 8, buffer.cone);

	buffer.ellipsoid = cl_create_buffer(scene.ellipsoid, CL_MEM_READ_ONLY, sizeof(*scene.ellipsoid) * (scene.world.nb_ellipsoid ? scene.world.nb_ellipsoid : 1));
	ret |= cl_set_buffer_arg(kernel, 9, buffer.ellipsoid);

	buffer.hyperboloid = cl_create_buffer(scene.hyperboloid, CL_MEM_READ_ONLY, sizeof(*scene.hyperboloid) * (scene.world.nb_hyperboloid ? scene.world.nb_hyperboloid : 1));
	ret |= cl_set_buffer_arg(kernel, 10, buffer.hyperboloid);

	return ((ret == 0) ? (kernel) : (ret));
}

cl_int		setup_lightning_kernel(t_display display, t_scene scene)
{
	int		ret = 0;
	cl_int	kernel;

	const char	*kernel_files[] =
	{
		"sources/opencl/kernel/lightning.cl",
	};

	kernel = cl_create_kernel(kernel_files, NB_ELEMENTS(kernel_files), LIGHTNING_KERNEL_NAME, KERNEL_COMPILE_FLAGS);

	ret |= cl_set_buffer_arg(kernel, 0, buffer.inters);

	buffer.light = cl_create_buffer(scene.light, CL_MEM_READ_ONLY, sizeof(*scene.light) * scene.world.nb_light);
	ret |= cl_set_buffer_arg(kernel, 1, buffer.light);
	ret |= cl_set_value_arg(kernel, 2, sizeof(scene.world), &scene.world);
	return ((ret == 0) ? (kernel) : (ret));
}

cl_int		setup_fill_image_kernel(t_display display)
{
	int		ret = 0;
	cl_int	kernel;

	const char	*kernel_files[] =
	{
		"sources/opencl/kernel/fill_image.cl",
	};

	kernel = cl_create_kernel(kernel_files, NB_ELEMENTS(kernel_files), FILL_IMAGE_KERNEL_NAME, KERNEL_COMPILE_FLAGS);
	
	buffer.texture = cl_create_gl_texture(display.gl_texture, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0);
	ret |= cl_set_gl_buffer_arg(kernel, 0, buffer.texture);

	ret |= cl_set_buffer_arg(kernel, 1, buffer.inters);

	ret |= cl_set_value_arg(kernel, 2, sizeof(display.width), &display.width);
	return ((ret == 0) ? (kernel) : (ret));
}
