#include "types.h"

__constant float g_tab[] =
{
	151, 160, 137, 91,  90,  15,  131, 13,  201, 95,  96,  53,  194, 233, 7,   225,
	140, 36,  103, 30,  69,  142, 8,   99,  37,  240, 21,  10,  23,  190, 6,   148,
	247, 120, 234, 75,  0,   26,  197, 62,  94,  252, 219, 203, 117, 35,  11,  32,
	57,  177, 33,  88,  237, 149, 56,  87,  174, 20,  125, 136, 171, 168, 68,  175,
	74,  165, 71,  134, 139, 48,  27,  166, 77,  146, 158, 231, 83,  111, 229, 122,
	60,  211, 133, 230, 220, 105, 92,  41,  55,  46,  245, 40,  244, 102, 143, 54,
	65,  25,  63,  161, 1,   216, 80,  73,  209, 76,  132, 187, 208, 89,  18,  169,
	200, 196, 135, 130, 116, 188, 159, 86,  164, 100, 109, 198, 173, 186, 3,   64,
	52,  217, 226, 250, 124, 123, 5,   202, 38,  147, 118, 126, 255, 82,  85,  212,
	207, 206, 59,  227, 47,  16,  58,  17,  182, 189, 28,  42,  223, 183, 170, 213,
	119, 248, 152, 2,   44,  154, 163, 70,  221, 153, 101, 155, 167, 43,  172, 9,
	129, 22,  39,  253, 19,  98,  108, 110, 79,  113, 224, 232, 178, 185, 112, 104,
	218, 246, 97,  228, 251, 34,  242, 193, 238, 210, 144, 12,  191, 179, 162, 241,
	81,  51,  145, 235, 249, 14,  239, 107, 49,  192, 214, 31,  181, 199, 106, 157,
	184, 84,  204, 176, 115, 121, 50,  45,  127, 4,   150, 254, 138, 236, 205, 93,
	222, 114, 67,  29,  24,  72,  243, 141, 128, 195, 78,  66,  215, 61,  156, 180
};

float	fade(float curve)
{
	return (curve * curve * curve * (curve * (curve * 6 - 15) + 10));
}

float	perl(float curve, float a, float b)
{
	return (mad(curve, (b - a), a));
}

void	init_noise(int3 *c_unit, float3 *p)
{
	float3 tmp = fmod(floor(*p), 256);
	*c_unit = (int3){tmp.x, tmp.y, tmp.z};
	*p -= floor(*p);
}

float	grad(int hash, float x, float y, float z)
{
	int		h = hash % 25;
	float	vec1;
	float	vec2;

	if (h < 8 || h == 12 || h == 13)
		vec1 = x;
	else
		vec1 = y;
	if (h < 4 || h == 12 || h == 13)
		vec2 = y;
	else
		vec2 = z;
	return (((h % 2) == 0 ? vec1 : -vec1) + ((h & 2) == 0 ? vec2 : -vec2));
}

float	get_perlin(float3 p)
{
	float3	vec;
	int3	c_unit;
	int		coor[6];

	init_noise(&c_unit, &p);
	vec = (p * p * p * (p * (p * 6 - 15) + 10)); /* fade */
	coor[0] = g_tab[c_unit.x] + c_unit.y;
	coor[1] = g_tab[coor[0]] + c_unit.z;
	coor[2] = g_tab[coor[0] + 1] + c_unit.z;
	coor[3] = g_tab[c_unit.x + 1] + c_unit.y;
	coor[4] = g_tab[coor[3]] + c_unit.z;
	coor[5] = g_tab[coor[3] + 1] + c_unit.z;
	float x1 = p.x - 1;
	float y1 = p.y - 1;
	float z1 = p.z - 1;
	return (perl(vec.z, perl(vec.y, perl(vec.x, grad(g_tab[coor[1]], p.x, p.y, p.z),
						grad(g_tab[coor[4]], x1, p.y, p.z)),
					perl(vec.x, grad(g_tab[coor[2]], p.x, y1, p.z),
						grad(g_tab[coor[5]], x1, y1, p.z))),
				perl(vec.y, perl(vec.x, grad(g_tab[coor[1] + 1], p.x, p.y, z1),
						grad(g_tab[coor[4] + 1], x1, p.y, z1)),
					perl(vec.x, grad(g_tab[coor[2] + 1], p.x, y1, z1),
						grad(g_tab[coor[5] + 1], x1, y1, z1)))));
}

/*
 * \param[in]	ray_origin	-> origin of the vector ray
 * \param[in]	ray_dir		-> direction of the ray
 * \param[in]	t			-> distance between ray_origin and intersection
 * \return					-> intersection coordinates
 */
inline float3	get_intersection(float3 ray_origin, float3 ray_dir, float t)
{
	return (mad(ray_dir, t, ray_origin));
}

/*
 * \param[in]		ray_dir		-> direction of the ray
 * \param[in]		ray_origin	-> image width
 * \param[in]		world		-> structure containing world informations
 * \param[in]		plane		-> structure containing planes informations
 * \param[in]		cylinder	-> structure containing cylinders informations
 * \param[in]		sphere		-> structure containing spheres informations
 * \param[in]		cone		-> structure containing cones informations
 * \param[out]		inters		-> structure containing intersections informations
 * \param[in/out]	tmin		-> minimal distance between ray origin and objects surface
 * \return						-> tmin changed ? true or false
 */
t_intersection	cast_ray(			float3			ray_dir,
									float3			ray_origin,
									t_world			world,
						__global	t_plane			*plane,
						__global	t_cylinder		*cylinder,
						__global	t_sphere		*sphere,
						__global	t_cone			*cone,
						__global	t_ellipsoid		*ellipsoid,
						__global	t_hyperboloid	*hyperboloid,
									float			*tmin)
{
	t_intersection	inters =
	{
		.obj_type = NONE
	};
	float3			dist;
	float			t = MAX_FLOAT;
	float			cos_height = 5;		// TODO: place this var in struct and parser
	float			wave_length = 5;	// TODO: place this var in struct and parser
	int				i;

	*tmin = MAX_FLOAT;
	for (i = 0; i < world.nb_plane; ++i)
	{
		dist = ray_origin - plane[i].pos;
		if (plane_intersect(ray_dir, dist, plane[i].dir, &t))
			if (t < *tmin)
			{
/*				inters.coord = get_intersection(ray_origin, ray_dir, t);
				inters.coord.y += 100 * cos(inters.coord.y / 10);
*/				inters.plane = plane[i];
				inters.obj_type = PLANE;
				inters.color = plane[i].color;
				*tmin = t;
			}
	}
	for (i = 0; i < world.nb_cylinder; ++i)
	{
		dist = ray_origin - cylinder[i].pos;
		if (cylinder_intersect(ray_dir, dist, cylinder[i].dir, cylinder[i].radius, &t))
			if (t < *tmin)
			{
				inters.cylinder = cylinder[i];
				inters.obj_type = CYLINDER;
				inters.color = cylinder[i].color;
				*tmin = t;
			}
	}
	for (i = 0; i < world.nb_sphere; ++i)
	{
		dist = ray_origin - sphere[i].pos;
		if (sphere_intersect(ray_dir, dist, sphere[i].dir, sphere[i].radius, &t))
			if (t < *tmin)
			{
				inters.sphere = sphere[i];
				inters.obj_type = SPHERE;
				inters.color = sphere[i].color;
				*tmin = t;
			}
	}
	for (i = 0; i < world.nb_cone; ++i)
	{
		dist = ray_origin - cone[i].pos;
		if (cone_intersect(ray_dir, dist, cone[i].dir, cone[i].angle, &t))
			if (t < *tmin)
			{
				inters.cone = cone[i];
				inters.obj_type = CONE;
				inters.color = cone[i].color;
				*tmin = t;
			}
	}
	for (i = 0; i < world.nb_ellipsoid; ++i)
	{
		dist = ray_origin - ellipsoid[i].pos;
		if (ellipsoid_intersect(ray_dir, dist, ellipsoid[i].dir, ellipsoid[i].radius, &t))
			if (t < *tmin)
			{
				inters.ellipsoid = ellipsoid[i];
				inters.obj_type = ELLIPSOID;
				inters.color = ellipsoid[i].color;
				*tmin = t;
			}
	}
	for (i = 0; i < world.nb_hyperboloid; ++i)
	{
		dist = ray_origin - hyperboloid[i].pos;
		if (hyperboloid_intersect(ray_dir, dist, hyperboloid[i].dir, hyperboloid[i].radius, hyperboloid[i].angle, &t))
			if (t < *tmin)
			{
				inters.hyperboloid = hyperboloid[i];
				inters.obj_type = HYPERBOLOID;
				inters.color = hyperboloid[i].color;
				*tmin = t;
			}
	}
	if (*tmin != MAX_FLOAT)
	{
		inters.coord = get_intersection(ray_origin, ray_dir, *tmin);
		inters.normal = get_normal(inters);
//		inters.color *= 0.2f * get_perlin(normalize(inters.coord)) * 20.f;
	}
	return (inters);
}

/*
 * \param[out]	inters	-> structure containing intersections informations
 * \param[in]	width	-> image width
 * \param[in]	height	-> image height
 * \param[in]	world	-> structure containing world informations
 * \param[in]	cam		-> structure containing camera informations
 * \param[in]	light	-> structure containing lights informations
 * \param[in]	plane	-> structure containing planes informations
 * \param[in]	cylinder-> structure containing cylinders informations.ambient_coef
 * \param[in]	sphere	-> structure containing spheres informations
 * \param[in]	cone	-> structure containing cones informations
 */
__kernel void   raytrace(__global		t_intersection	*inters,
										ushort			width,
										ushort			height,
										t_world			world,
										t_cam			cam,
						__global		t_plane			*plane,
						__global		t_cylinder		*cylinder,
						__global		t_sphere		*sphere,
						__global		t_cone			*cone,
						__global		t_ellipsoid		*ellipsoid,
						__global		t_hyperboloid	*hyperboloid)
{
	size_t	gid = get_global_id(0);
	float	tmin;

	// Vectors
	int2	pos = (int2){gid % width, gid / width};
	float3	ray = (float3){	-MID(width) + pos.x,
							-MID(height) + pos.y,
							-PLANEDIST(width, 120)};

	inters[gid] = cast_ray(ray, cam.pos, world, plane, cylinder, sphere, cone, ellipsoid, hyperboloid, &tmin);
}
