bool	solve_quadratic(float a, float b, float c, float *t)
{
	float	delta, x1, x2;

	delta = b * b - 4 * a * c;
	if (delta < 0.f || a == 0.f)
		return (false);

	x1 = (-b - half_sqrt(delta)) / (2.f * a);
	x2 = (-b + half_sqrt(delta)) / (2.f * a);
	*t = min(x1, x2);
	*t = (*t < 0) ? (MAX_FLOAT) : (*t);

	return (true);
}

void	rotate_object(float3 *ray, float3 *dist, float3 dir)
{
/*	float3	tmp_ray = *ray;
	float3	rot =
	{
		-atan2(dir.x, dir.y) * 180 / M_PI,
		-atan2(dir.y, dir.z) * 180 / M_PI,
		-atan2(dir.z, dir.x) * 180 / M_PI
	};
	
	// X matrix
	tmp_ray.y = (cos(rot.x) * ray->y) + (-sin(rot.x) * ray->z);
	tmp_ray.z = (sin(rot.x) * ray->y) + (cos(rot.x) * ray->z);
	
	// Y matrix
	tmp_ray.x = (cos(rot.y) * ray->x) + (sin(rot.y) * tmp_ray.z);
	tmp_ray.z = (-sin(rot.y) * ray->x) + (cos(rot.y) * tmp_ray.z);
	
	// Z matrix
//	tmp_ray.x = (cos(rot.z) * tmp_ray.x) + (-sin(rot.z) * tmp_ray.y);
//	tmp_ray.y = (sin(rot.z) * tmp_ray.x) + (cos(rot.z) * tmp_ray.y);
	
	
	if (get_global_id(0) == 0)
	{
		printf("angle   = %f, %f, %f\n", rot.x, rot.y, rot.z);
		printf("ray     = %f, %f, %f\n", ray->x, ray->y, ray->z);
	}
	*ray = tmp_ray;
	if (get_global_id(0) == 0)
	{
		printf("new_ray = %f, %f, %f\n", ray->x, ray->y, ray->z);
	}*/
/*	*ray = *ray - (dir * dot(*ray, dir));
	*dist = *dist - (dir * dot(*dist, dir));*/
	float3	angle =
	{
		-atan2(dir.x, dir.y) * 180 / M_PI,
		-atan2(dir.y, dir.z) * 180 / M_PI,
		-atan2(dir.z, dir.x) * 180 / M_PI
	};
	float3 v = *ray;
	float tmpx = v.x;
	float tmpy = v.y;
	float cosx = cos(angle.x);
	float cosy = cos(angle.y);
	float cosz = cos(angle.z);
	float sinx = sin(angle.x);
	float siny = sin(angle.y);
	float sinz = sin(angle.z);
	v.y = (v.y * cosx) - (v.z * sinx);
	v.z = (tmpy * sinx) + (v.z * cosx);
	v.x = (v.x * cosy) + (v.z * siny);
	v.z = (tmpx * -siny) + (v.z * cosy);
	tmpx = v.x;
	v.x = (v.x * cosz) - (v.y * sinz);
	v.y = (tmpx * sinz) + (v.y * cosz);
	*ray = v;
}

bool	hyperboloid_intersect(float3 ray, float3 dist, float3 dir, float radius, float angle, float *t)
{
	float	a, b, c, delta, x1, x2;
	
	float tmpsin2 = sin2(angle * M_PI / 180);
	float tmpcos2 = cos2(angle * M_PI / 180);
	float dotdist = dot(dist, dir);
	float dotray = dot(ray, dir);
	float radius2 = (radius < 0) ? -(radius * radius) : (radius * radius);

	ray = ray - (dir * dotray);
	dist = dist - (dir * dotdist);

	a = tmpcos2 * dot(ray, ray) - tmpsin2 * dotray * dotray;
	b = 2.f * tmpcos2 * dot(ray, dist)
		-2.f * tmpsin2 * dotray * dotdist;
	c = tmpcos2 * dot(dist, dist) - tmpsin2 * dotdist * dotdist - radius2;

	return (solve_quadratic(a, b, c, t));
}

bool	plane_intersect(float3 ray, float3 dist, float3 dir, float *t)
{
	float	x1;

	x1 = -dot(dist, dir) / dot(ray, dir);
	*t = (x1 < 0.f) ? (MAX_FLOAT) : (x1);

	return ((x1 >= 0.f) ? (true) : (false));
}

bool	cone_intersect(float3 ray, float3 dist, float3 dir, float angle, float *t)
{
	float	a, b, c, delta, x1, x2;

	float tmpsin2 = sin2(angle * M_PI / 180);
	float tmpcos2 = cos2(angle * M_PI / 180);
	float dotdist = dot(dist, dir);
	float dotray = dot(ray, dir);

	ray = ray - (dir * dotray);
	dist = dist - (dir * dotdist);

	a = tmpcos2 * dot(ray, ray) - tmpsin2 * dotray * dotray;
	b = 2.f * tmpcos2 * dot(ray, dist)
		-2.f * tmpsin2 * dotray * dotdist;
	c = tmpcos2 * dot(dist, dist) - tmpsin2 * dotdist * dotdist;
	
	return (solve_quadratic(a, b, c, t));
}

bool	cylinder_intersect(float3 ray, float3 dist, float3 dir, float radius, float *t)
{
	float	a, b, c, delta, x1, x2;
	
	float dotdist = dot(dist, dir);
	float dotray = dot(ray, dir);
	
	ray = ray - (dir * dot(ray, dir));
	dist = dist - (dir * dot(dist, dir));
	
	a = dot(ray, ray);
	b = 2.f * dot(ray, dist);
	c = dot(dist, dist) - (radius * radius);
	
	return (solve_quadratic(a, b, c, t));
}

bool	sphere_intersect(float3 ray, float3 dist, float3 dir, float radius, float *t)
{
	float	a, b, c, delta, x1, x2;

	a = dot(ray, ray);
	b = 2.f * dot(ray, dist);
	c = dot(dist, dist) - (radius * radius);

	return (solve_quadratic(a, b, c, t));
}

bool	ellipsoid_intersect(float3 ray, float3 dist, float3 dir, float3 radius, float *t)
{
	float	a, b, c, delta, x1, x2;

	a =	((ray.x * ray.x) / (radius.x * radius.x))
	+	((ray.y * ray.y) / (radius.y * radius.y))
	+	((ray.z * ray.z) / (radius.z * radius.z));
	b =	((2.f * dist.x * ray.x) / (radius.x * radius.x))
	+	((2.f * dist.y * ray.y) / (radius.y * radius.y))
	+	((2.f * dist.z * ray.z) / (radius.z * radius.z));
	c =	((dist.x * dist.x) / (radius.x * radius.x))
	+	((dist.y * dist.y) / (radius.y * radius.y))
	+	((dist.z * dist.z) / (radius.z * radius.z)) - 1.f;

	return (solve_quadratic(a, b, c, t));
}
