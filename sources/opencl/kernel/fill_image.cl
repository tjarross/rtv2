#include "types.h"

/*
 * \param[out]	img		-> opengl texture to fill
 * \param[in]	inters	-> structure containing intersections informations
 * \param[in]	width	-> image width
 */
__kernel void   fill_image(__write_only		image2d_t		img,
							__global		t_intersection	*inters,
											ushort			width)
{
	size_t	gid = get_global_id(0);
	int2	pos = (int2){gid % width, gid / width};
	
	write_imagef(img, pos, inters[gid].color);
}
