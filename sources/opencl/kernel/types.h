#ifndef TYPES_H
# define TYPES_H

#define MAX_FLOAT				3.40282347e38f
#define MID(x)					(x / 2)
#define PLANEDIST(width, fov)	(width / (2 * tan(fov / 2.f)))

typedef enum	e_light_type
{
	POINT,
	PARALLEL
}				t_light_type;

typedef enum	e_obj_type
{
	NONE,
	PLANE,
	CYLINDER,
	SPHERE,
	CONE,
	ELLIPSOID,
	HYPERBOLOID
}				t_obj_type;

typedef struct	s_cam
{
	float3		pos;	// between -FLT_MAX	and	FLT_MAX
	float3		dir;	// between -FLT_MAX	and	FLT_MAX
}				t_cam;

typedef struct	s_world
{
	float		ambient_coef;	// between 0.f		and	1.f
	float		filter_coef;	// between 0.f		and	1.f
	uint		filter_color;	// between 0x000000	and	0xFFFFFF
	uint		antialiasing;	// between 1		and	7
	uint		nb_reflection;	// between 0		and	255
	uint		nb_refraction;	// between 0		and	255

	uint		nb_light;		// between 1	and	4096
	uint		nb_plane;		// between 1	and	4096
	uint		nb_cylinder;	// between 1	and	4096
	uint		nb_sphere;		// between 1	and	4096
	uint		nb_cone;		// between 1	and	4096
	uint		nb_ellipsoid;	// between 1	and	4096
	uint		nb_hyperboloid;	// between 1	and	4096
}				t_world;

typedef struct		s_light
{
	float3			pos;				// between -FLT_MAX	and	FLT_MAX
	t_light_type	type;
	float			luminosity_coef;	// between 0.f		and	1.f
	float4			color;				// between 0x000000	and	0xFFFFFF
}					t_light;

typedef struct	s_plane
{
	float3		pos;				// between -FLT_MAX	and	FLT_MAX
	float3		dir;				// between -FLT_MAX	and	FLT_MAX
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	float		checkerboard_coef;	// between 0.f		and	1.f
	uint		checkerboard_color;	// between 0x000000	and	0xFFFFFF
	float4		color; 				// between 0x000000	and	0xFFFFFF
}				t_plane;

typedef struct	s_cylinder
{
	float3		pos;				// between -FLT_MAX	and	FLT_MAX
	float3		dir;				// between -FLT_MAX	and	FLT_MAX
	float		radius;				// between 0		and	INT_MAX
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	float4		color;				// between 0x000000 and	0xFFFFFF
}				t_cylinder;

typedef struct	s_sphere
{
	float3		pos;				// between -FLT_MAX	and	FLT_MAX
	float3		dir;				// between -FLT_MAX	and	FLT_MAX
	float		radius;				// between 0		and	FLT_MAX
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	float4		color;				// between 0x000000	and	0xFFFFFF
}				t_sphere;

typedef struct	s_cone
{
	float3		pos;				// between -FLT_MAX	and	FLT_MAX
	float3		dir;				// between -FLT_MAX	and	FLT_MAX
	float		angle;				// between 0.f		and	90.f
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	float4		color;				// between 0x000000	and	0xFFFFFF
}				t_cone;

typedef struct	s_ellipsoid
{
	float3		pos;				// between -FLT_MAX	and	FLT_MAX
	float3		dir;				// between -FLT_MAX	and	FLT_MAX
	float3		radius;				// between 0.f		and	90.f
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	float4		color;				// between 0x000000	and	0xFFFFFF
}				t_ellipsoid;

typedef struct	s_hyperboloid
{
	float3		pos;				// between -FLT_MAX	and	FLT_MAX
	float3		dir;				// between -FLT_MAX	and	FLT_MAX
	float		radius;				// between 0		and	FLT_MAX
	float		angle;				// between 0.f		and	90.f
	float		shine_coef;			// between 0.f		and	1.f
	uint		specular_power;		// between 0		and	INT_MAX
	float		reflection_coef;	// between 0.f		and	1.f
	float		refraction_coef;	// between 0.f		and	1.f
	float4		color;				// between 0x000000	and	0xFFFFFF
}				t_hyperboloid;

typedef struct	s_intersection
{
	float3	coord;
	float3	normal;
	t_obj_type	obj_type;
	union
	{
		t_plane			plane;
		t_cylinder		cylinder;
		t_sphere		sphere;
		t_cone			cone;
		t_ellipsoid		ellipsoid;
		t_hyperboloid	hyperboloid;
	};
	float4	color;
}				t_intersection;

float	cos2(float angle) { return ((1 + cos(2 * angle)) / 2.f); }
float	sin2(float angle) { return ((1 - cos(2 * angle)) / 2.f); }

#endif /* TYPES_H */
