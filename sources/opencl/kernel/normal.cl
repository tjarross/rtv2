#include "types.h"

float3	project(float3 a, float3 b)
{
	float t = dot(b, b);

	if (t)
		return (b * ((dot(a, b)) / (t)));
	return (a);
}

float3	get_normal(t_intersection inters)
{
	switch (inters.obj_type)
	{
		case PLANE:
			return (inters.plane.dir);
		case CYLINDER:
			return (normalize(((inters.coord - inters.cylinder.pos) - project((inters.coord - inters.cylinder.pos), inters.cylinder.dir))));
		case SPHERE:
			return (normalize(inters.coord - inters.sphere.pos));
		case CONE:
			return (normalize(((inters.coord - inters.cone.pos) - project((inters.coord - inters.cone.pos), inters.cone.dir))));
		case ELLIPSOID:
			return (normalize(2.f * (inters.coord - inters.ellipsoid.pos) / (inters.ellipsoid.radius * inters.ellipsoid.radius)));
		case HYPERBOLOID:
			return (normalize(((inters.coord - inters.hyperboloid.pos) - project((inters.coord - inters.hyperboloid.pos), inters.hyperboloid.dir))));
		default:
			return ((float3){0, 0, 0});
	}
}
