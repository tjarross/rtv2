#include "types.h"

__kernel void	lightning(__global	t_intersection	*inters,
						__global	t_light			*light,
									t_world			world)
{
	size_t	gid = get_global_id(0);
	uint	i;

	float4 obj_color = inters[gid].color;
	inters[gid].color = (float4){0, 0, 0, 0};
	// Beer-Lambert's law
	for (i = 0; i < world.nb_light; ++i)
	{
		float4	new_color;
		float3	ray = normalize(light[i].pos - inters[gid].coord);
		float	incident_angle = max(0.f, dot(inters[gid].normal, ray));
		new_color = (obj_color + light[i].color * light[i].luminosity_coef) * incident_angle;
		inters[gid].color += clamp(new_color, world.ambient_coef * inters[gid].color, 1.f);
	}
	min(inters[gid].color, 1.f);
}
