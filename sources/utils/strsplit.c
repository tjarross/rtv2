#include "utils.h"

static int	count_words(const char *str, char delim)
{
	int i;
	int	word;

	i = -1;
	word = 0;
	while (str[++i])
	{
		if (str[i] != delim)
		{
			++word;
			while (str[i] != delim && str[i])
				++i;
			if (!str[i])
				return (word);
		}
	}
	return (word);
}

static int	get_wordlen(const char *str, char delim)
{
	int	i;

	i = 0;
	while (str[i] != delim && str[i])
		++i;
	return (i);
}

char		**strsplit(const char *str, char delim)
{
	char	**dest;
	int		words;
	int		i;
	int		j;
	int		word_len;

	i = 0;
	j = -1;
	if (str == NULL || !isascii(delim) || !(words = count_words(str, delim)))
		return (NULL);
	if ((dest = (char **)malloc(sizeof(char *) * (words + 1))) == NULL)
		return (NULL);
	dest[words] = NULL;
	while (++j < words)
	{
		while (str[i] == delim)
			++i;
		word_len = get_wordlen(&str[i], delim);
		if ((dest[j] = (char *)malloc(sizeof(char) * (word_len + 1))) == NULL)
			return (NULL);
		dest[j] = strncpy(dest[j], &str[i], word_len);
		dest[j][word_len] = '\0';
		i += word_len;
	}
	return (dest);
}