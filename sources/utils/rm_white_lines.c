#include "utils.h"

static uint	length_str_tab(char **tab)
{
	uint	i = 0;

	while (tab[i])
		i++;
	return (i);
}

static uint	find_whitespaces(const char *str)
{
	uint	i = 0;

	while (str[i])
	{
		if (isgraph(str[i]) != 0)
			return (1);
		i++;
	}
	return (0);
}

char				**rm_white_lines(char **src)
{
	char	**dest;
	uint	size;
	uint	i = 0;
	uint	j = 0;

	size = length_str_tab(src);
	if ((dest = (char **)malloc(sizeof(char *) * size + 1)) == NULL)
		return (NULL);
	while (src[i])
	{
		if (find_whitespaces(src[i]))
			if ((dest[j++] = strdup(src[i])) == NULL)
				return (NULL);
		i++;
	}
	dest[size] = NULL;
	free_str_tab(src);
	return (dest);
}
