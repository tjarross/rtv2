#include "utils.h"

int		clamp(int nbr, int min, int max)
{
	nbr = (nbr > max) ? max : nbr;
	nbr = (nbr < min) ? min : nbr;
	return (nbr);
}

long	clampl(long nbr, long min, long max)
{
	nbr = (nbr > max) ? max : nbr;
	nbr = (nbr < min) ? min : nbr;
	return (nbr);
}

float	clampf(float nbr, float min, float max)
{
	nbr = (nbr > max) ? max : nbr;
	nbr = (nbr < min) ? min : nbr;
	return (nbr);
}
