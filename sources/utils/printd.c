#include "utils.h"

int		printd(UNUSED const char *format, ...)
{
#ifdef DEBUG
	va_list	arg_list;
	int		ret;

	va_start(arg_list, format);
	ret = vfprintf(stderr, format, arg_list);
	va_end(arg_list);
	return (ret);
#endif
	return (0);
}
