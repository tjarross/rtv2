#include "utils.h"

void	free_str_tab(char **tab)
{
	for (uint i = 0; tab[i]; ++i)
		free(tab[i]);
	free(tab);
}
