#include "utils.h"

char	**get_file(char *pathname)
{
	char	str[MAX_SOURCE_SIZE];
	FILE	*fp;

	if ((fp = fopen(pathname, "r")) == NULL)
		return (NULL);
	if (fread(str, sizeof(char), MAX_SOURCE_SIZE, fp) == 0)
		return (NULL);
	if (fclose(fp) != 0)
		return (NULL);
	return (strsplit(str, '\n'));
}
