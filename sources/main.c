#include "rtv2.h"
#include "parser.h"
#include "utils.h"

#include "opengl.h"
#include "libcl.h"

t_opencl_buffer	buffer;

void	execute_kernels(uint nb_kernels, ...)
{
	cl_int	kernel;
	va_list	list;

	va_start(list, nb_kernels);
	
	clock_t	time = clock();
	for (int i = 0; i < nb_kernels - 1; ++i)
	{
		int ret = cl_exec_kernel(va_arg(list, cl_int), WIDTH * HEIGHT);
	}
	cl_exec_interop_kernel(va_arg(list, cl_int), WIDTH * HEIGHT);
	printf("Render takes %f seconds\n", (clock() - time) / (float)CLOCKS_PER_SEC);
	cl_finish_kernel_exec();
	va_end(list);
}

GLsizei	load_vao_vbo(t_display *display)
{
	float	screen_corners[] =
	{
		-1.f,  1.f, 0.f,
		 1.f,  1.f, 0.f,
		-1.f, -1.f, 0.f,
		 1.f, -1.f, 0.f,
	};
	
	uint	corner_indices[] =
	{
		0, 1, 2,
		3, 1, 2,
	};
	
	glGenVertexArrays(1, &display->vao);
	glBindVertexArray(display->vao);
	
	// OpenGL Screen Corners
	glGenBuffers(1, &display->gl_screen_corners);
	glBindBuffer(GL_ARRAY_BUFFER, display->gl_screen_corners);
	glBufferData(GL_ARRAY_BUFFER, sizeof(screen_corners), screen_corners, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	
	// OpenGL Element Buffer(*display).
	glGenBuffers(1, &display->gl_corners_ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, display->gl_corners_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(corner_indices), corner_indices, GL_STATIC_DRAW);

	// Render Texture
	glGenTextures(1, &display->gl_texture);
	glBindTexture(GL_TEXTURE_2D, (*display).gl_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WIDTH, HEIGHT, 0, GL_BGRA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	return (sizeof(corner_indices));
}

int		main(int argc, char **argv)
{
	t_display		display;
	GLenum			gl_err;
	GLsizei			nb_vertices;

	cl_int			intersection_kernel;
	cl_int			fill_image_kernel;
	cl_int			lightning_kernel;

	t_scene 		scene;

	char 			py_command[PATH_MAX];

	display.width = WIDTH;
    display.height = HEIGHT;

    if (argc != 2)
    {
        printf("usage: ./rtv2 scene.rt\n");
        return (EXIT_FAILURE);
    }

    snprintf(py_command, PATH_MAX, "/usr/bin/python3 sources/parser/python/check_syntax.py %s", argv[1]);
    if (system(py_command))
        return (EXIT_FAILURE);

    printf("\n\033[22;32mPROGRAMM START IN C \033[0m\n");
    scene = parser(argv[1]);
	display_parser_infos(scene);

	if (!(display.window = glfw_init(WINDOW_TITLE)))
		return (EXIT_FAILURE);

	glewExperimental = GL_TRUE;
	if ((gl_err = glewInit()) != GLEW_OK)
	{
		printf("GLEW Error: %s\n", glewGetErrorString(gl_err));
		glfwTerminate();
		return (EXIT_FAILURE);
	}

	display.program = load_program(VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH);
	glUseProgram(display.program);

	nb_vertices = load_vao_vbo(&display);

	// OpenCL
	cl_interop_init(display.window);
	intersection_kernel = setup_intersection_kernel(display, scene);
	lightning_kernel = setup_lightning_kernel(display, scene);
	fill_image_kernel = setup_fill_image_kernel(display);
	execute_kernels(3,	intersection_kernel,
						lightning_kernel,
						fill_image_kernel);

	while (!glfwWindowShouldClose(display.window))
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDrawElements(GL_TRIANGLES, nb_vertices, GL_UNSIGNED_INT, 0);
		glfwSwapBuffers(display.window);
		glfwPollEvents();
	}

	cl_exit();

	glDisableVertexAttribArray(0);
	glDeleteBuffers(1, &display.gl_screen_corners);
	glDeleteTextures(1, &display.gl_texture);
	glDeleteVertexArrays(1, &display.vao);

	glfwTerminate();

	return (EXIT_SUCCESS);
}
