#include "parser.h"

typedef enum	e_index
{
	POS,
	LUMI_COEF,
	COLOR,

	NB_INDEXES
}				t_index;

t_light			*get_light(uint *nb_light, char **file, uint i)
{
	t_light *light;
	char 	*occurence;
	uint 	i_field;
	uint	counter[NB_INDEXES] = {0};
	uint	tmp_color;
	char 	*fields[] = 
	{
		"Position",
		"Luminosity_coef",
		"Color"
	};

	*nb_light = clamp(get_number_entity(file[i]), 1, MAX_ENTITIES);
	if ((light = (t_light *)malloc(sizeof(t_light) * *nb_light)) == NULL)
		return (NULL);
	bzero(light, sizeof(t_light));
	while (strstr(file[i], "}") == NULL)
	{
		i_field = 0;
		while (i_field < NB_ELEMENTS(fields))
		{
			if ((occurence = strcasestr(file[i], fields[i_field])))
			{
				if (i_field == POS)
					fill_vector3f(&light[counter[POS]++].pos, file, i + 1);
				else if (i_field == LUMI_COEF)
					light[counter[LUMI_COEF]++].luminosity_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == COLOR)
				{
					tmp_color = clamp(atof(&file[i][get_number_index(file[i])]), 0, 0xffffff);
					light[counter[COLOR]].color.x = RED(tmp_color) / 255.f;
					light[counter[COLOR]].color.y = GREEN(tmp_color) / 255.f;
					light[counter[COLOR]].color.z = BLUE(tmp_color) / 255.f;
					light[counter[COLOR]++].color.w = ALPHA(tmp_color);
				}
				break ;
			}
			i_field++;
		}
		i++;
	}
	return (light);
}
