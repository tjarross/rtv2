#include "parser.h"

t_world	get_world(char **file, uint i)
{
	t_world world;
	char	*occurence;
	uint	i_field;
	char	*fields[] = 
	{
		"Ambient_coef",
		"Filter_coef",
		"Filter_color",
		"Antialiasing",
		"Nb_reflection",
		"Nb_refraction"
	};

	bzero(&world, sizeof(t_world));
	while (file[i] && strstr(file[i], "}") == NULL)
	{
		i_field = 0;
		while (i_field < NB_ELEMENTS(fields))
		{
			if ((occurence = strcasestr(file[i], fields[i_field])))
			{
				if (i_field == 0)
					world.ambient_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == 1)
					world.filter_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == 2)
					world.filter_color = clamp(atof(&file[i][get_number_index(file[i])]), 0, 0xffffff);
				else if (i_field == 3)
					world.antialiasing = clamp(atoi(&file[i][get_number_index(file[i])]), 1, 7);
				else if (i_field == 4)
					world.nb_reflection = clamp(atoi(&file[i][get_number_index(file[i])]), 1, 255);
				else
					world.nb_refraction = clamp(atoi(&file[i][get_number_index(file[i])]), 1, 255);
				break ;
			}
			i_field++;
		}
		i++;
	}
	return (world);
}
