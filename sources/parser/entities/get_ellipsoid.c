#include "parser.h"

typedef enum	e_index
{
	POS,
	DIR,
	RADIUS,
	SHINE_COEF,
	SPEC_POW,
	REFLE_COEF,
	REFRA_COEF,
	COLOR,

	NB_INDEXES
}				t_index;

t_ellipsoid		*get_ellipsoid(uint *nb_ellipsoid, char **file, uint i)
{
	t_ellipsoid	*ellipsoid;
	char 		*occurence;
	uint 		i_field;
	uint		counter[NB_INDEXES] = {0};
	uint		tmp_color;
	char 		*fields[] = 
	{
		"Position",
		"Direction",
		"Radius",
		"Shine_coef",
		"Specular_power",
		"Reflection_coef",
		"Refraction_coef",
		"Color"
	};
	
	*nb_ellipsoid = clampl(get_number_entity(file[i]), 1, MAX_ENTITIES);
	if ((ellipsoid = (t_ellipsoid *)malloc(sizeof(t_ellipsoid) * *nb_ellipsoid)) == NULL)
		return (NULL);
	bzero(ellipsoid, sizeof(t_ellipsoid));
	while (strstr(file[i], "}") == NULL)
	{
		i_field = 0;
		while (i_field < NB_ELEMENTS(fields))
		{
			if ((occurence = strcasestr(file[i], fields[i_field])))
			{
				if (i_field == POS)
					fill_vector3f(&ellipsoid[counter[POS]++].pos, file, i + 1);
				else if (i_field == DIR)
					fill_vector3f(&ellipsoid[counter[DIR]++].dir, file, i + 1);
				else if (i_field == RADIUS)
					fill_vector3f(&ellipsoid[counter[RADIUS]++].radius, file, i + 1);
				else if (i_field == SHINE_COEF)
					ellipsoid[counter[SHINE_COEF]++].shine_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == SPEC_POW)
					ellipsoid[counter[SPEC_POW]++].specular_power = clamp(atoi(&file[i][get_number_index(file[i])]), 0, INT_MAX);
				else if (i_field == REFLE_COEF)
					ellipsoid[counter[REFLE_COEF]++].reflection_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == REFRA_COEF)
					ellipsoid[counter[REFRA_COEF]++].refraction_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == COLOR)
				{
					tmp_color = clamp(atof(&file[i][get_number_index(file[i])]), 0, 0xffffff);
					ellipsoid[counter[COLOR]].color.x = RED(tmp_color) / 255.f;
					ellipsoid[counter[COLOR]].color.y = GREEN(tmp_color) / 255.f;
					ellipsoid[counter[COLOR]].color.z = BLUE(tmp_color) / 255.f;
					ellipsoid[counter[COLOR]++].color.w = ALPHA(tmp_color);
				}
				break ;
			}
			i_field++;
		}
		i++;
	}
	return (ellipsoid);
}
