#include "parser.h"

t_cam	get_cam(char **file, uint i)
{
	t_cam	cam;
	char	*occurence;
	uint	i_field;
	char	*fields[] = 
	{
		"Position",
		"Direction"
	};

	bzero(&cam, sizeof(t_cam));
	while (file[i] && strstr(file[i], "}") == NULL)
	{
		i_field = 0;
		while (i_field < NB_ELEMENTS(fields))
		{
			if ((occurence = strcasestr(file[i], fields[i_field])))
			{
				if (i_field == 0)
					fill_vector3f(&cam.pos, file, i + 1);
				else if (i_field == 1)
					fill_vector3f(&cam.dir, file, i + 1);
				break ;
			}
			i_field++;
		}
		i++;
	}
	return (cam);
}
