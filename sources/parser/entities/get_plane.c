#include "parser.h"

typedef enum	e_index
{
	POS,
	DIR,
	SHINE_COEF,
	SPEC_POW,
	REFLE_COEF,
	REFRA_COEF,
	CHKERBRD_COEF,
	CHKERBRD_COLOR,
	COLOR,

	NB_INDEXES
}				t_index;

t_plane			*get_plane(uint *nb_plane, char **file, uint i)
{
	t_plane *plane;
	char 	*occurence;
	uint 	i_field;
	uint	counter[NB_INDEXES] = {0};
	uint	tmp_color;
	char 	*fields[] = 
	{
		"Position",
		"Direction",
		"Shine_coef",
		"Specular_power",
		"Reflection_coef",
		"Refraction_coef",
		"Checkerboard_coef",
		"Checkerboard_color",
		"Color"
	};
	
	*nb_plane = clampl(get_number_entity(file[i]), 1, MAX_ENTITIES);
	if ((plane = (t_plane *)malloc(sizeof(t_plane) * *nb_plane)) == NULL)
		return (NULL);
	bzero(plane, sizeof(t_plane));
	while (strstr(file[i], "}") == NULL)
	{
		i_field = 0;
		while (i_field < NB_ELEMENTS(fields))
		{
			if ((occurence = strcasestr(file[i], fields[i_field])))
			{
				if (i_field == POS)
					fill_vector3f(&plane[counter[POS]++].pos, file, i + 1);
				else if (i_field == DIR)
					fill_vector3f(&plane[counter[DIR]++].dir, file, i + 1);
				else if (i_field == SHINE_COEF)
					plane[counter[SHINE_COEF]++].shine_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == SPEC_POW)
					plane[counter[SPEC_POW]++].specular_power = clamp(atoi(&file[i][get_number_index(file[i])]), 0, INT_MAX);
				else if (i_field == REFLE_COEF)
					plane[counter[REFLE_COEF]++].reflection_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == REFRA_COEF)
					plane[counter[REFRA_COEF]++].refraction_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == CHKERBRD_COEF)
					plane[counter[CHKERBRD_COEF]++].checkerboard_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == CHKERBRD_COLOR)
					plane[counter[CHKERBRD_COLOR]++].checkerboard_color = clamp(atof(&file[i][get_number_index(file[i])]), 0, 0xffffff);
				else if (i_field == COLOR)
				{
					tmp_color = clamp(atof(&file[i][get_number_index(file[i])]), 0, 0xffffff);
					plane[counter[COLOR]].color.x = RED(tmp_color) / 255.f;
					plane[counter[COLOR]].color.y = GREEN(tmp_color) / 255.f;
					plane[counter[COLOR]].color.z = BLUE(tmp_color) / 255.f;
					plane[counter[COLOR]++].color.w = ALPHA(tmp_color);
				}
				break ;
			}
			i_field++;
		}
		i++;
	}
	return (plane);
}
