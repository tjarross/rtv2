#include "parser.h"

typedef enum	e_index
{
	POS,
	DIR,
	RADIUS,
	ANGLE,
	SHINE_COEF,
	SPEC_POW,
	REFLE_COEF,
	REFRA_COEF,
	COLOR,

	NB_INDEXES
}				t_index;

t_hyperboloid		*get_hyperboloid(uint *nb_hyperboloid, char **file, uint i)
{
	t_hyperboloid 	*hyperboloid;
	char 			*occurence;
	uint 			i_field;
	uint			counter[NB_INDEXES] = {0};
	uint			tmp_color;
	char 			*fields[] = 
	{
		"Position",
		"Direction",
		"Radius",
		"Angle",
		"Shine_coef",
		"Specular_power",
		"Reflection_coef",
		"Refraction_coef",
		"Color"
	};
	
	*nb_hyperboloid = clampl(get_number_entity(file[i]), 1, MAX_ENTITIES);
	if ((hyperboloid = (t_hyperboloid *)malloc(sizeof(t_hyperboloid) * *nb_hyperboloid)) == NULL)
		return (NULL);
	bzero(hyperboloid, sizeof(t_hyperboloid));
	while (strstr(file[i], "}") == NULL)
	{
		i_field = 0;
		while (i_field < NB_ELEMENTS(fields))
		{
			if ((occurence = strcasestr(file[i], fields[i_field])))
			{
				if (i_field == POS)
					fill_vector3f(&hyperboloid[counter[POS]++].pos, file, i + 1);
				else if (i_field == DIR)
					fill_vector3f(&hyperboloid[counter[DIR]++].dir, file, i + 1);
				else if (i_field == RADIUS)
					hyperboloid[counter[RADIUS]++].radius = clampf(atof(&file[i][get_number_index(file[i])]), -FLT_MAX, FLT_MAX);
				else if (i_field == ANGLE)
					hyperboloid[counter[ANGLE]++].angle = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 90.f);
				else if (i_field == SHINE_COEF)
					hyperboloid[counter[SHINE_COEF]++].shine_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == SPEC_POW)
					hyperboloid[counter[SPEC_POW]++].specular_power = clamp(atoi(&file[i][get_number_index(file[i])]), 0, INT_MAX);
				else if (i_field == REFLE_COEF)
					hyperboloid[counter[REFLE_COEF]++].reflection_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == REFRA_COEF)
					hyperboloid[counter[REFRA_COEF]++].refraction_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == COLOR)
				{
					tmp_color = clamp(atof(&file[i][get_number_index(file[i])]), 0, 0xffffff);
					hyperboloid[counter[COLOR]].color.x = RED(tmp_color) / 255.f;
					hyperboloid[counter[COLOR]].color.y = GREEN(tmp_color) / 255.f;
					hyperboloid[counter[COLOR]].color.z = BLUE(tmp_color) / 255.f;
					hyperboloid[counter[COLOR]++].color.w = ALPHA(tmp_color);
				}
				break ;
			}
			i_field++;
		}
		i++;
	}
	return (hyperboloid);
}
