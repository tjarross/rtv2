#include "parser.h"

typedef enum	e_index
{
	POS,
	DIR,
	ANGLE,
	SHINE_COEF,
	SPEC_POW,
	REFLE_COEF,
	REFRA_COEF,
	COLOR,

	NB_INDEXES
}				t_index;

t_cone			*get_cone(uint *nb_cone, char **file, uint i)
{
	t_cone 	*cone;
	char 	*occurence;
	uint 	i_field;
	uint	counter[NB_INDEXES] = {0};
	uint	tmp_color;
	char 	*fields[] = 
	{
		"Position",
		"Direction",
		"Angle",
		"Shine_coef",
		"Specular_power",
		"Reflection_coef",
		"Refraction_coef",
		"Color"
	};
	
	*nb_cone = clampl(get_number_entity(file[i]), 1, MAX_ENTITIES);
	if ((cone = (t_cone *)malloc(sizeof(t_cone) * *nb_cone)) == NULL)
		return (NULL);
	bzero(cone, sizeof(t_cone));
	while (strstr(file[i], "}") == NULL)
	{
		i_field = 0;
		while (i_field < NB_ELEMENTS(fields))
		{
			if ((occurence = strcasestr(file[i], fields[i_field])))
			{
				if (i_field == POS)
					fill_vector3f(&cone[counter[POS]++].pos, file, i + 1);
				else if (i_field == DIR)
					fill_vector3f(&cone[counter[DIR]++].dir, file, i + 1);
				else if (i_field == ANGLE)
					cone[counter[ANGLE]++].angle = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 90.f);
				else if (i_field == SHINE_COEF)
					cone[counter[SHINE_COEF]++].shine_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == SPEC_POW)
					cone[counter[SPEC_POW]++].specular_power = clamp(atoi(&file[i][get_number_index(file[i])]), 0, INT_MAX);
				else if (i_field == REFLE_COEF)
					cone[counter[REFLE_COEF]++].reflection_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == REFRA_COEF)
					cone[counter[REFRA_COEF]++].refraction_coef = clampf(atof(&file[i][get_number_index(file[i])]), 0.f, 1.f);
				else if (i_field == COLOR)
				{
					tmp_color = clamp(atof(&file[i][get_number_index(file[i])]), 0, 0xffffff);
					cone[counter[COLOR]].color.x = RED(tmp_color) / 255.f;
					cone[counter[COLOR]].color.y = GREEN(tmp_color) / 255.f;
					cone[counter[COLOR]].color.z = BLUE(tmp_color) / 255.f;
					cone[counter[COLOR]++].color.w = ALPHA(tmp_color);
				}
				break ;
			}
			i_field++;
		}
		i++;
	}
	return (cone);
}
