#include "parser.h"

long	get_number_entity(char *line)
{
	long	number = 0;
	char	*p = line;

	while (*p)
	{
		if (isdigit(*p))
			number = strtol(p, &p, 10);
		else
			p++;
	}
	return (number);
}
