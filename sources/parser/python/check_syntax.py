import sys
import re
import parser_messages_error as error

""" to write in color in the shell """
CYAN =      '\033[22;36m'
PINK =      '\033[22;35m'
YELLOW =    '\033[22;33m'
RED =       '\033[22;31m'

""" for getting error messages easily """
ERROR = {'CAM':			error.CAM,
		'WORLD':		error.WORLD,
		'LIGHT':		error.LIGHT,
		'PLANE':		error.PLANE,
		'CYLINDER':		error.CYLINDER,
		'SPHERE':		error.SPHERE,
		'CONE':			error.CONE,
		'ELLIPSOID':	error.ELLIPSOID,
		'HYPERBOLOID':	error.HYPERBOLOID}

""" 'define' for regex pattern """
X = r'x=(-)?(\d+)(\.)?(\d+)?;'
Y = r'y=(-)?(\d+)(\.)?(\d+)?;'
Z = r'z=(-)?(\d+)(\.)?(\d+)?;'

def error_exit(error):
    print(error)
    sys.exit(1)

class CheckerEntity:

    def __init__(self, file, size_file):
        self.file = file
        self.size_file = size_file
        self.is_valid = 0

    def loop(self, index, regex, goal, name, nb_obj=1, nb_ellipsoid=1):
        print('nb: ' + str(nb_obj))    # number of objects that are about to be parsed
        i = index
        if re.search(r'{}', self.file[i]) is not None:
            """ the scop is empty """
            error_exit('Your ' + name + ' scop is empty')

        match = []
        if re.search(r'{', self.file[i]) is not None:
            """ go through the scop """
            while i < self.size_file and re.search(r'}', self.file[i]) is None:

                if ('POS' in regex
                    and re.match(regex['POS'][0], self.file[i + 0], flags=re.IGNORECASE) is not None
                    and re.match(regex['POS'][1], self.file[i + 1], flags=re.IGNORECASE) is not None
                    and re.match(regex['POS'][2], self.file[i + 2], flags=re.IGNORECASE) is not None
                    and re.match(regex['POS'][3], self.file[i + 3], flags=re.IGNORECASE) is not None):
                    """ field 'Position:' identified """
                    goal['POS'] += 1

                if ('DIR' in regex
                    and re.match(regex['DIR'][0], self.file[i + 0], flags=re.IGNORECASE) is not None
                    and re.match(regex['DIR'][1], self.file[i + 1], flags=re.IGNORECASE) is not None
                    and re.match(regex['DIR'][2], self.file[i + 2], flags=re.IGNORECASE) is not None
                    and re.match(regex['DIR'][3], self.file[i + 3], flags=re.IGNORECASE) is not None):
                    """ field 'Direction:' identified """
                    goal['DIR'] += 1

                if ('RAD' in regex
                    and re.match(regex['RAD'][0], self.file[i + 0], flags=re.IGNORECASE) is not None
                    and re.match(regex['RAD'][1], self.file[i + 1], flags=re.IGNORECASE) is not None
                    and re.match(regex['RAD'][2], self.file[i + 2], flags=re.IGNORECASE) is not None
                    and re.match(regex['RAD'][3], self.file[i + 3], flags=re.IGNORECASE) is not None):
                    """ field 'Direction:' identified """
                    goal['RAD'] += 1

                if 'SPE' in regex:
                    """ loop for specific fields """
                    for r in regex['SPE']:
                        if re.match(r, self.file[i], flags=re.IGNORECASE) is not None:
                            """ specific fields identified """
                            match.append(r)
                            goal['SPE'] += 1
                i += 1

        if 'SPE' in regex:
            unmatch = list(set(regex['SPE']) - set(match))
            for r in unmatch:
                print(RED + "WARNING:\033[0m field : '" + r.split('=',1)[0] + "' is missing. Set by default to 0")

        if i == self.size_file:
            """ we're at the end of the file without ever meeting '}' """
            error_exit('You need to close your ' + name + " scop with '}'")

        self.valid_goal(goal, regex, name, nb_obj, nb_ellipsoid)
        return i

    def Cam(self, index):
        goal    = {'POS': 0,                       'DIR': 0}
        regex   = {'POS': [r'Position:', X, Y, Z], 'DIR': [r'Direction:', X, Y, Z]}
        return self.loop(index, regex, goal, 'CAM')

    def World(self, index):
        pattern = [r'Ambient_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Filter_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Filter_color=(\w+);',
                   r'Antialiasing=(-)?(\d+);',
                   r'Nb_reflection=(-)?(\d+);',
                   r'Nb_refraction=(-)?(\d+);']
        
        goal    = {'SPE': 0}
        regex   = {'SPE': pattern}
        return self.loop(index, regex, goal, 'WORLD')

    def Light(self, index, nb_light):
        nb_light = self.check_nb(nb_light)
        pattern = [r'Type=[a-zA-Z]+;',
                   r'Luminosity_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Color=(\w+);']

        goal    = {'POS': 0,                       'SPE': 0}
        regex   = {'POS': [r'Position:', X, Y, Z], 'SPE': pattern}
        return self.loop(index, regex, goal, 'LIGHT', nb_light)

    def Plane(self, index, nb_plane):
        nb_plane = self.check_nb(nb_plane)
        pattern = [r'Shine_coef=(-)?(\d+)(\.)?(\d+)?;', 
                   r'Specular_power=(-)?(\d+);', 
                   r'Reflection_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Refraction_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Checkerboard_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Checkerboard_color=(\w+);',
                   r'Color=(\w+);']

        goal    = {'POS': 0,                       'DIR': 0,                        'SPE': 0}
        regex   = {'POS': [r'Position:', X, Y, Z], 'DIR': [r'Direction:', X, Y, Z], 'SPE': pattern}
        return self.loop(index, regex, goal, 'PLANE', nb_plane)

    def Cylinder(self, index, nb_cylinder):
        nb_cylinder = self.check_nb(nb_cylinder)
        pattern = [r'Radius=(-)?(\d+)(\.)?(\d+)?;',
                   r'Shine_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Specular_power=(-)?(\d+);',
                   r'Reflection_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Refraction_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Color=(\w+);']

        goal    = {'POS': 0,                       'DIR': 0,                        'SPE': 0}
        regex   = {'POS': [r'Position:', X, Y, Z], 'DIR': [r'Direction:', X, Y, Z], 'SPE': pattern}
        return self.loop(index, regex, goal, 'CYLINDER', nb_cylinder)

    def Sphere(self, index, nb_sphere):
        nb_sphere = self.check_nb(nb_sphere)
        pattern = [r'Radius=(-)?(\d+)(\.)?(\d+)?;',
                   r'Shine_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Specular_power=(-)?(\d+);',
                   r'Reflection_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Refraction_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Color=(\w+);']

        goal    = {'POS': 0,                       'DIR': 0,                        'SPE': 0}
        regex   = {'POS': [r'Position:', X, Y, Z], 'DIR': [r'Direction:', X, Y, Z], 'SPE': pattern}
        return self.loop(index, regex, goal, 'SPHERE', nb_sphere)

    def Cone(self, index, nb_cone):
        nb_cone = self.check_nb(nb_cone)
        pattern = [r'Angle=(-)?(\d+)(\.)?(\d+)?;',
                   r'Shine_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Specular_power=(-)?(\d+);',
                   r'Reflection_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Refraction_coef=(-)?(\d+)(\.)?(\d+)?;', 
                   r'Color=(\w+);']

        goal    = {'POS': 0,                       'DIR': 0,                        'SPE': 0}
        regex   = {'POS': [r'Position:', X, Y, Z], 'DIR': [r'Direction:', X, Y, Z], 'SPE': pattern}
        return self.loop(index, regex, goal, 'CONE', nb_cone)

    def Ellipsoid(self, index, nb_ellipsoid):
        nb_ellipsoid = self.check_nb(nb_ellipsoid)
        pattern = [r'Shine_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Specular_power=(-)?(\d+);',
                   r'Reflection_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Refraction_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Color=(\w+);']

        goal    = {'POS': 0,                       'DIR': 0,                        'RAD': 0,                     'SPE': 0}
        regex   = {'POS': [r'Position:', X, Y, Z], 'DIR': [r'Direction:', X, Y, Z], 'RAD': [r'Radius:', X, Y, Z], 'SPE': pattern}
        return self.loop(index, regex, goal, 'ELLIPSOID', nb_ellipsoid)

    def Hyperboloid(self, index, nb_hyperboloid):
        nb_hyperboloid = self.check_nb(nb_hyperboloid)
        pattern = [r'Radius=(-)?(\d+)(\.)?(\d+)?;',
                   r'Angle=(-)?(\d+)(\.)?(\d+)?;',
                   r'Shine_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Specular_power=(-)?(\d+);',
                   r'Reflection_coef=(-)?(\d+)(\.)?(\d+)?;',
                   r'Refraction_coef=(-)?(\d+)(\.)?(\d+)?;', 
                   r'Color=(\w+);']

        goal    = {'POS': 0,                       'DIR': 0,                        'SPE': 0}
        regex   = {'POS': [r'Position:', X, Y, Z], 'DIR': [r'Direction:', X, Y, Z], 'SPE': pattern}
        return self.loop(index, regex, goal, 'HYPERBOLOID', nb_hyperboloid)

    def check_nb(self, nb_obj):
        if nb_obj == []:
            """ the parentheses of 'Sphere ()' are empty for example """
            nb_obj = [1,]   # so we set to 1
        return int(nb_obj[0])

    def valid_goal(self, goal, regex, name, nb_obj, nb_ellipsoid):
        print(goal, end='\n\n') # result of self.loop()
        if ('POS' in regex and goal['POS'] != nb_obj    # the number of 'Position:' fields is not equal to the number of objects
            or 'DIR' in regex and goal['DIR'] != nb_obj # the number of 'Direction:' fields is not equal to the number of objects
            or 'SPE' in regex and goal['SPE'] != len(regex['SPE']) * nb_obj # the number of specific fields is not equal to the number of objects
            or 'RAD' in regex and goal['RAD'] != nb_ellipsoid): # the number of 'Radius' fields is not equal to the number of ellipsoids
            error_exit(ERROR[name])
        if name == 'CAM' or name == 'WORLD':
            """ Cam () or World () found and correct """
            self.is_valid += 1

    def valid_file(self):
        if self.is_valid != 2:
            """ there is no Cam () and/or World () in the secene.rt """
            error_exit("You need to have Cam () and World ()")



def check_syntax(file, size_file):
    i = 0
    checker = CheckerEntity(file, size_file)
    for i in range(size_file):
        if re.match(r'Cam\(\)', file[i], flags=re.IGNORECASE) is not None:
            print(PINK + 'Cam () \033[0m')
            i = checker.Cam(i + 1)
        elif re.match(r'World\(\)', file[i], flags=re.IGNORECASE) is not None:
            print(PINK + 'World () \033[0m')
            i = checker.World(i + 1)
        elif re.match(r'Light(s)?\((\d+)?\)', file[i], flags=re.IGNORECASE) is not None:
            print(YELLOW + 'Light () \033[0m')
            i = checker.Light(i + 1, re.findall(r'\d+', file[i]))
        elif re.match(r'Plane(s)?\((\d+)?\)', file[i], flags=re.IGNORECASE) is not None:
            print(YELLOW + 'Plane () \033[0m')
            i = checker.Plane(i + 1, re.findall(r'\d+', file[i]))
        elif re.match(r'Cylinder(s)?\((\d+)?\)', file[i], flags=re.IGNORECASE) is not None:
            print(YELLOW + 'Cylinder () \033[0m')
            i = checker.Cylinder(i + 1, re.findall(r'\d+', file[i]))
        elif re.match(r'Sphere(s)?\((\d+)?\)', file[i], flags=re.IGNORECASE) is not None:
            print(YELLOW + 'Sphere () \033[0m')
            i = checker.Sphere(i + 1, re.findall(r'\d+', file[i]))
        elif re.match(r'Cone(s)?\((\d+)?\)', file[i], flags=re.IGNORECASE) is not None:
            print(YELLOW + 'Cone () \033[0m')
            i = checker.Cone(i + 1, re.findall(r'\d+', file[i]))
        elif re.match(r'Ellipsoid(s)?\((\d+)?\)', file[i], flags=re.IGNORECASE) is not None:
            print(YELLOW + 'Ellipsoid () \033[0m')
            i = checker.Ellipsoid(i + 1, re.findall(r'\d+', file[i]))
        elif re.match(r'Hyperboloid(s)?\((\d+)?\)', file[i], flags=re.IGNORECASE) is not None:
            print(YELLOW + 'Hyperboloid () \033[0m')
            i = checker.Hyperboloid(i + 1, re.findall(r'\d+', file[i]))
    checker.valid_file()

if __name__ == '__main__':
    if sys.argv[1].find('.rt') == -1:
        error_exit('you must use a .rt file')
    print('\n' + CYAN + 'PROGRAMM START IN PYTHON \033[0m')
    with open(sys.argv[1], 'r') as file:
        content = file.read()                                   # get the file content
        file = list(filter(str.strip, content.split('\n')))     # split the content arround '\n'
        pattern = re.compile(r'\s+')                            # create regex pattern for find all whitespaces
        clean_file = []
        for line in file:
            clean_file.append(pattern.sub('', line))            # remove all whitespaces for all lines of the file with our pattern
        check_syntax(clean_file, len(clean_file))
    sys.exit(0)
