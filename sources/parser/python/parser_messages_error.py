CAM = """Syntax error in your Cam scop, here is an example:
Cam ()
{
    Position:
        x = 0.5; // between FLT_MIN and FLT_MAX
        y = 120; // between FLT_MIN and FLT_MAX
        z = 8.2; // between FLT_MIN and FLT_MAX
    Direction:
        x = 0.8; // between FLT_MIN and FLT_MAX
        y = 0.;  // between FLT_MIN and FLT_MAX
        z = 10;  // between FLT_MIN and FLT_MAX
}"""

WORLD = """Syntax error in your World scop, here is an example:
World ()
{
    Ambient_coef = 0.2;         // between 0 and 1
    Filter_coef = 0.4;          // between 0 and 1
    Filter_color = 0xffffff;    // between 0x000000 and 0xffffff
    Antialiasing = 2;           // between 1 and 7
    Nb_reflection = 50;         // between 0 and 255
    Nb_refraction = 10;         // between 0 and 255
}"""

LIGHT = """Syntax error in your Light scop, here is an example:
Lights (2)
{
    Position:
        x = 1400;               // between FLT_MIN and FLT_MAX
        y = 1200;               // between FLT_MIN and FLT_MAX
        z = 1800;               // between FLT_MIN and FLT_MAX
    Type = Point                // Point or Parallel
    Luminosity_coef = 0.2;      // between 0 and 1
    Color = 0xff6347;           // between 0x000000 and 0xffffff

    Position:
        x = -1400;
        y = 1200;
        z = 1800;
    Type = Parallel
    Luminosity_coef = 0.6;
    Color = 0x1e90ff;
}
"""

PLANE = """Syntax error in your Plane scop, here is an example:
Plane (1)
{
    Position:
        x = 0;
        y = -1000;
        z = 0;
    Direction:
        x = 0;
        y = 1;
        z = 0;
    Shine_coef = 0.8;
    Specular_power = 12;
    Reflection_coef = 0.6;
    Refraction_coef = 0;
    Checkerboard_coef = 0.5;
    Checkerboard_color = 0x654321;
    Color = 0x123456;
}
"""

CYLINDER = """Syntax error in your Cylinder scop, here is an example:
Cylinder (1)
{
    Position:
        x = 500;
        y = 0;
        z = 300;
    Direction:
        x = 0;
        y = 1;
        z = 0;
    Radius = 50;
    Shine_coef = 1;
    Specular_power = 1;
    Reflection_coef = 0.2;
    Refraction_coef = 0;
    Color = 0x385743;
}
"""

SPHERE = """Syntax error in your Sphere scop, here is an example:
Sphere (1)
{
    Position:
        x = 0;
        y = 500;
        z = 0;
    Direction:
        x = 0;
        y = 1;
        z = 0;
    Raduis = 500;
    Shine_coef = 0.8;
    Specular_power = 16;
    Reflection_coef = 0.2;
    Refraction_coef = 0;
    Color = 0x995533;
}
"""

CONE = """Syntax error in your Cone scop, here is an example:
Cones (2)
{
    Position:
        x = 500;
        y = -500;
        z = -500;
    Direction:
        x = 1;
        y = 0;
        z = 0;
    Angle = 50;
    Shine_coef = 1;
    Specular_power = 16;
    Reflection_coef = 0.2;
    Refraction_coef = 0;
    Color = 0x753658;

    Position:
        x = 0;
        y = 100;
        z = 0;
    Direction:
        x = 0;
        y = 1;
        z = 0;
    Angle = 75;
    Shine_coef = 1;
    Specular_power = 10;
    Reflection_coef = 0.2;
    Refraction_coef = 0;
    Color = 0xff0000;
}
"""

ELLIPSOID = """Syntax error in your Cone scop, here is an example:
Ellipsoids (2)
{
    Position:
        x = 500;
        y = -500;
        z = -500;
    Direction:
        x = 1;
        y = 0;
        z = 0;
    Radius:
        x = 500;
        y = 200;
        z = 300;
    Shine_coef = 1;
    Specular_power = 16;
    Reflection_coef = 0.2;
    Refraction_coef = 0;
    Color = 0x753658;

    Position:
        x = 0;
        y = 100;
        z = 0;
    Direction:
        x = 0;
        y = 1;
        z = 0;
    Radius:
        x = 500;
        y = 200;
        z = 300;
    Shine_coef = 1;
    Specular_power = 10;
    Reflection_coef = 0.2;
    Refraction_coef = 0;
    Color = 0xff0000;
}
"""

HYPERBOLOID = """Syntax error in your Cone scop, here is an example:
Hyperboloids (2)
{
    Position:
        x = 500;
        y = -500;
        z = -500;
    Direction:
        x = 1;
        y = 0;
        z = 0;
    Radius = 50;
    Angle = 70
    Shine_coef = 1;
    Specular_power = 16;
    Reflection_coef = 0.2;
    Refraction_coef = 0;
    Color = 0x753658;

    Position:
        x = 0;
        y = 100;
        z = 0;
    Direction:
        x = 0;
        y = 1;
        z = 0;
	Radius = 250
    Angle = 75;
    Shine_coef = 1;
    Specular_power = 10;
    Reflection_coef = 0.2;
    Refraction_coef = 0;
    Color = 0xff0000;
}
"""
