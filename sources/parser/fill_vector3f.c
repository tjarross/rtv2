#include "parser.h"

void fill_vector3f(cl_float3 *field, char **scene, uint i)
{
	field->x = clampf(atof(&scene[i][get_number_index(scene[i])]), -FLT_MAX, FLT_MAX);
	field->y = clampf(atof(&scene[i + 1][get_number_index(scene[i + 1])]), -FLT_MAX, FLT_MAX);
	field->z = clampf(atof(&scene[i + 2][get_number_index(scene[i + 2])]), -FLT_MAX, FLT_MAX);
}
