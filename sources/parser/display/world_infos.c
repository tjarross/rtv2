#include "parser.h"

void world_infos(t_scene scn)
{
	printf("World ()\n");
	printf("\tAmbient_coef = %f\n", scn.world.ambient_coef);
	printf("\tFilter_coef = %f\n", scn.world.filter_coef);
	printf("\tFilter_color = %d\n", scn.world.filter_color);
	printf("\tAntialiasing = %d\n", scn.world.antialiasing);
	printf("\tNb_reflection = %d\n", scn.world.nb_reflection);
	printf("\tNb_refraction = %d\n\n", scn.world.nb_refraction);
}
