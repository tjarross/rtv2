#include "parser.h"

void plane_infos(t_scene scn)
{
	uint	i = 0;

	printf("Plane ()\n");
	while (i < scn.world.nb_plane)
	{
		printf("\tPosition:\n");
		printf("\tx = %f\n", scn.plane[i].pos.x);
		printf("\ty = %f\n", scn.plane[i].pos.y);
		printf("\tz = %f\n", scn.plane[i].pos.z);
		printf("\tDirection:\n");
		printf("\tx = %f\n", scn.plane[i].dir.x);
		printf("\ty = %f\n", scn.plane[i].dir.y);
		printf("\tz = %f\n", scn.plane[i].dir.z);
		printf("\tShine_coef = %f\n", scn.plane[i].shine_coef);
		printf("\tSpecular_power = %d\n", scn.plane[i].specular_power);
		printf("\tReflection_coef = %f\n", scn.plane[i].reflection_coef);
		printf("\tRefraction_coef = %f\n", scn.plane[i].refraction_coef);
		printf("\tCheckerboard_coef = %f\n", scn.plane[i].checkerboard_coef);
		printf("\tCheckerboard_color = %d\n", scn.plane[i].checkerboard_color);
		printf("\tColor = 0x%x%x%x%x\n\n",	(int)(scn.plane[i].color.x * 255),
											(int)(scn.plane[i].color.y * 255),
											(int)(scn.plane[i].color.z * 255),
											(int)(scn.plane[i].color.w * 255));
		i++;
	}
}
