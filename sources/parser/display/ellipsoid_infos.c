#include "parser.h"

void ellipsoid_infos(t_scene scn)
{
	uint	i = 0;

	printf("Ellipsoid ()\n");
	while (i < scn.world.nb_ellipsoid)
	{
		printf("\tPosition:\n");
		printf("\tx = %f\n", scn.ellipsoid[i].pos.x);
		printf("\ty = %f\n", scn.ellipsoid[i].pos.y);
		printf("\tz = %f\n", scn.ellipsoid[i].pos.z);
		printf("\tDirection:");
		printf("\tx = %f\n", scn.ellipsoid[i].dir.x);
		printf("\ty = %f\n", scn.ellipsoid[i].dir.y);
		printf("\tz = %f\n", scn.ellipsoid[i].dir.z);
		printf("\tRadius:\n");
		printf("\tx = %f\n", scn.ellipsoid[i].radius.x);
		printf("\ty = %f\n", scn.ellipsoid[i].radius.y);
		printf("\tz = %f\n", scn.ellipsoid[i].radius.z);
		printf("\tShine_coef = %f\n", scn.ellipsoid[i].shine_coef);
		printf("\tSpecular_power = %d\n", scn.ellipsoid[i].specular_power);
		printf("\tReflection_coef = %f\n", scn.ellipsoid[i].reflection_coef);
		printf("\tRefraction_coef = %f\n", scn.ellipsoid[i].refraction_coef);
		printf("\tColor = %x%x%x%x\n\n",	(int)(scn.ellipsoid[i].color.x * 255),
											(int)(scn.ellipsoid[i].color.y * 255),
											(int)(scn.ellipsoid[i].color.z * 255),
											(int)(scn.ellipsoid[i].color.w * 255));
		i++;
	}
}
