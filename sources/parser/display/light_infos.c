#include "parser.h"

void light_infos(t_scene scn)
{
	uint	i = 0;

	printf("Light ()\n");
	while (i < scn.world.nb_light)
	{
		printf("\tPosition:\n");
		printf("\tx = %f\n", scn.light[i].pos.x);
		printf("\ty = %f\n", scn.light[i].pos.y);
		printf("\tz = %f\n", scn.light[i].pos.z);
		// printf("\tType = %f\n", scn.light[i].type);
		printf("\tluminosity_coef = %f\n", scn.light[i].luminosity_coef);
		printf("\tColor = %x%x%x%x\n\n",	(int)(scn.light[i].color.x * 255),
											(int)(scn.light[i].color.y * 255),
											(int)(scn.light[i].color.z * 255),
											(int)(scn.light[i].color.w * 255));
		i++;
	}
}
