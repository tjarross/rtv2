#include "parser.h"

void cone_infos(t_scene scn)
{
	uint	i = 0;

	printf("Cone ()\n");
	while (i < scn.world.nb_cone)
	{
		printf("\tPosition:\n");
		printf("\tx = %f\n", scn.cone[i].pos.x);
		printf("\ty = %f\n", scn.cone[i].pos.y);
		printf("\tz = %f\n", scn.cone[i].pos.z);
		printf("\tDirection:\n");
		printf("\tx = %f\n", scn.cone[i].dir.x);
		printf("\ty = %f\n", scn.cone[i].dir.y);
		printf("\tz = %f\n", scn.cone[i].dir.z);
		printf("\tAngle = %f\n", scn.cone[i].angle);
		printf("\tShine_coef = %f\n", scn.cone[i].shine_coef);
		printf("\tSpecular_power = %d\n", scn.cone[i].specular_power);
		printf("\tReflection_coef = %f\n", scn.cone[i].reflection_coef);
		printf("\tRefraction_coef = %f\n", scn.cone[i].refraction_coef);
		printf("\tColor = %x%x%x%x\n\n",	(int)(scn.cone[i].color.x * 255),
											(int)(scn.cone[i].color.y * 255),
											(int)(scn.cone[i].color.z * 255),
											(int)(scn.cone[i].color.w * 255));
		i++;
	}
}
