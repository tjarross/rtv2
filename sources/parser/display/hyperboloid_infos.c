#include "parser.h"

void hyperboloid_infos(t_scene scn)
{
	uint	i = 0;

	printf("Hyperboloid ()\n");
	while (i < scn.world.nb_hyperboloid)
	{
		printf("\tPosition:\n");
		printf("\tx = %f\n", scn.hyperboloid[i].pos.x);
		printf("\ty = %f\n", scn.hyperboloid[i].pos.y);
		printf("\tz = %f\n", scn.hyperboloid[i].pos.z);
		printf("\tDirection:\n");
		printf("\tx = %f\n", scn.hyperboloid[i].dir.x);
		printf("\ty = %f\n", scn.hyperboloid[i].dir.y);
		printf("\tz = %f\n", scn.hyperboloid[i].dir.z);
		printf("\tRadius = %f\n", scn.hyperboloid[i].radius);
		printf("\tAngle = %f\n", scn.hyperboloid[i].angle);
		printf("\tShine_coef = %f\n", scn.hyperboloid[i].shine_coef);
		printf("\tSpecular_power = %d\n", scn.hyperboloid[i].specular_power);
		printf("\tReflection_coef = %f\n", scn.hyperboloid[i].reflection_coef);
		printf("\tRefraction_coef = %f\n", scn.hyperboloid[i].refraction_coef);
		printf("\tColor = %x%x%x%x\n\n",	(int)(scn.hyperboloid[i].color.x * 255),
											(int)(scn.hyperboloid[i].color.y * 255),
											(int)(scn.hyperboloid[i].color.z * 255),
											(int)(scn.hyperboloid[i].color.w * 255));
		i++;
	}
}
