#include "parser.h"

void sphere_infos(t_scene scn)
{
	uint	i = 0;

	printf("Sphere ()\n");
	while (i < scn.world.nb_sphere)
	{
		printf("\tPosition:\n");
		printf("\tx = %f\n", scn.sphere[i].pos.x);
		printf("\ty = %f\n", scn.sphere[i].pos.y);
		printf("\tz = %f\n", scn.sphere[i].pos.z);
		printf("\tDirection:");
		printf("\tx = %f\n", scn.sphere[i].dir.x);
		printf("\ty = %f\n", scn.sphere[i].dir.y);
		printf("\tz = %f\n", scn.sphere[i].dir.z);
		printf("\tRadius = %f\n", scn.sphere[i].radius);
		printf("\tShine_coef = %f\n", scn.sphere[i].shine_coef);
		printf("\tSpecular_power = %d\n", scn.sphere[i].specular_power);
		printf("\tReflection_coef = %f\n", scn.sphere[i].reflection_coef);
		printf("\tRefraction_coef = %f\n", scn.sphere[i].refraction_coef);
		printf("\tColor = %x%x%x%x\n\n",	(int)(scn.sphere[i].color.x * 255),
											(int)(scn.sphere[i].color.y * 255),
											(int)(scn.sphere[i].color.z * 255),
											(int)(scn.sphere[i].color.w * 255));
		i++;
	}
}
