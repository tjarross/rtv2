#include "parser.h"

void cam_infos(t_scene scn)
{
	printf("Cam ()\n");
	printf("\tPosition:\n");
	printf("\tx = %f\n", scn.cam.pos.x);
	printf("\ty = %f\n", scn.cam.pos.y);
	printf("\tz = %f\n", scn.cam.pos.z);
	printf("\tDirection:\n");
	printf("\tx = %f\n", scn.cam.dir.x);
	printf("\ty = %f\n", scn.cam.dir.y);
	printf("\tz = %f\n\n", scn.cam.dir.z);
}
