#include "parser.h"

void cylinder_infos(t_scene scn)
{
	uint	i = 0;

	printf("Cylinder ()\n");
	while (i < scn.world.nb_cylinder)
	{
		printf("\tPosition:\n");
		printf("\tx = %f\n", scn.cylinder[i].pos.x);
		printf("\ty = %f\n", scn.cylinder[i].pos.y);
		printf("\tz = %f\n", scn.cylinder[i].pos.z);
		printf("\tDirection:\n");
		printf("\tx = %f\n", scn.cylinder[i].dir.x);
		printf("\ty = %f\n", scn.cylinder[i].dir.y);
		printf("\tz = %f\n", scn.cylinder[i].dir.z);
		printf("\tRadius = %f\n", scn.cylinder[i].radius);
		printf("\tShine_coef = %f\n", scn.cylinder[i].shine_coef);
		printf("\tSpecular_power = %d\n", scn.cylinder[i].specular_power);
		printf("\tReflection_coef = %f\n", scn.cylinder[i].reflection_coef);
		printf("\tRefraction_coef = %f\n", scn.cylinder[i].refraction_coef);
		printf("\tColor = %x%x%x%x\n\n",	(int)(scn.cylinder[i].color.x * 255),
											(int)(scn.cylinder[i].color.x * 255),
											(int)(scn.cylinder[i].color.x * 255),
											(int)(scn.cylinder[i].color.x * 255));
		i++;
	}
}
