#include "parser.h"

void display_parser_infos(t_scene scn)
{
	cam_infos(scn);
	world_infos(scn);
	light_infos(scn);
	plane_infos(scn);
	cylinder_infos(scn);
	sphere_infos(scn);
	cone_infos(scn);
	ellipsoid_infos(scn);
	hyperboloid_infos(scn);
}
