#include "parser.h"

t_scene parser(char *pathname)
{
	t_scene	scene;
	uint	i = 0;
	uint	i_field;
	char	**file;
	char	*occurence;
	char	*fields[] =
	{
		"Cam",
		"World",
		"Light",
		"Plane",
		"Cylinder",
		"Sphere",
		"Cone",
		"Ellipsoid",
		"Hyperboloid"
	};

	bzero(&scene, sizeof(t_scene));
	if ((file = rm_white_lines(get_file(pathname))) == NULL)
		return (scene);

	while (file[i])
	{
		i_field = 0;
		while (i_field < NB_ELEMENTS(fields))
		{
			if ((occurence = strcasestr(file[i], fields[i_field])))
			{
				if (i_field == 0)
					scene.cam = get_cam(file, i);
				else if (i_field == 1)
					scene.world = get_world(file, i);
				else if (i_field == 2)
					scene.light = get_light(&scene.world.nb_light, file, i);
				else if (i_field == 3)
					scene.plane = get_plane(&scene.world.nb_plane, file, i);
				else if (i_field == 4)
					scene.cylinder = get_cylinder(&scene.world.nb_cylinder, file, i);
				else if (i_field == 5)
					scene.sphere = get_sphere(&scene.world.nb_sphere, file, i);
				else if (i_field == 6)
					scene.cone = get_cone(&scene.world.nb_cone, file, i);
				else if (i_field == 7)
					scene.ellipsoid = get_ellipsoid(&scene.world.nb_ellipsoid, file, i);
				else if (i_field == 8)
					scene.hyperboloid = get_hyperboloid(&scene.world.nb_hyperboloid, file, i);
				break ;
			}
			i_field++;
		}
		i++;
	}
	free_str_tab(file);
	return (scene);
}
