#include "parser.h"

uint get_number_index(char *line)
{
	for (uint i = 0; line[i]; ++i)
		if (isdigit(line[i]) || line[i] == '-')
			return (i);
	return (0);
}
