#version 330

layout (location = 0) in vec3 coords;

out vec2 UV;

void main(void)
{
	gl_Position = vec4(coords, 1.f);
	UV = (coords.xy + 1) / 2.f;		// Replaced coords (-1.f, 1.f) to (0.f, 1.f)
}
