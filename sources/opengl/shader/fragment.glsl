#version 330

in	vec2	UV;
out	vec4	out_color;

uniform	sampler2D	texture_data;

void main(void)
{
	out_color = texture(texture_data, UV);
}
