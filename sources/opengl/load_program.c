#include "opengl.h"

static char			*get_content_file(int fd)
{
	struct stat	stat;
	char		*content;

	if (fstat(fd, &stat) == -1)
		return (NULL);
	if ((content = (char *)malloc(stat.st_size + 1)) == NULL)
		return (NULL);
	if (read(fd, content, stat.st_size) != stat.st_size)
		return (NULL);
	content[stat.st_size] = '\0';
	return (content);
}

static void 		print_shader_log(GLuint shader)
{
	GLint	status;

	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		char buffer[LOG_STATUS_BUFFER_LEN];

		glGetShaderInfoLog(shader, sizeof(buffer), NULL, buffer);
		printf("Shader error: %s\n", buffer);
	}
}

// TODO : remove get_content_file() and get the content with FILE* method
static GLuint		load_shader(GLenum shader_type, const char *path)
{
	GLchar	*shader_source;
	GLuint	shader;
	int		fd;

	if ((shader = glCreateShader(shader_type)) == 0)
		return (0);
	if ((fd = open(path, O_RDONLY)) == -1)
		return (0);
	if ((shader_source = get_content_file(fd)) == NULL)
		return (0);
	close(fd);
	glShaderSource(shader, 1, (const GLchar **)&shader_source, NULL);
	glCompileShader(shader);
	printf("%s\n", path);
	print_shader_log(shader);
	free(shader_source);
	return (shader);
}

GLuint				load_program(const char *vertex_shader_pathname, const char *fragment_shader_pathname)
{
	GLuint	program;

	program = glCreateProgram();
	printf("Loaded shaders:\n");
	glAttachShader(program, load_shader(GL_VERTEX_SHADER, vertex_shader_pathname));
	glAttachShader(program, load_shader(GL_FRAGMENT_SHADER, fragment_shader_pathname));
	glLinkProgram(program);
	return (program);
}
