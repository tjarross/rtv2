#include "opengl.h"

void	error_callback(int error, const char *msg)
{
	printf("GLFW Error (%d): %s\n", error, msg);
}

void	key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	(void)scancode;
	(void)mode;

	if (action == GLFW_PRESS)
	{
		switch (key)
		{
			case GLFW_KEY_ESCAPE:
				glfwSetWindowShouldClose(window, GL_TRUE);
				break ;
		}
	}
}
